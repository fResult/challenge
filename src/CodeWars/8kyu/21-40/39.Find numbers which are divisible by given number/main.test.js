import { divisibleBy } from './main';

describe('8 Kyu: Find numbers which are divisible by given number', () => {
  it('When input [1, 2, 3, 4, 5, 6] and 2 should return [2, 4, 6]', () => {
    expect(divisibleBy([1, 2, 3, 4, 5, 6], 2)).toEqual([2, 4, 6]);
  });
  it('When input [1, 2, 3, 4, 5, 6] and 3 should return [3, 6]', () => {
    expect(divisibleBy([1, 2, 3, 4, 5, 6], 3)).toEqual([3, 6]);
  });
  it('When input [0, 1, 2, 3, 4, 5, 6] and 4 should return [0, 4]', () => {
    expect(divisibleBy([0, 1, 2, 3, 4, 5, 6], 4)).toEqual([0, 4]);
  });
  it('When input [0] and 4 should return [0]', () => {
    expect(divisibleBy([0], 4)).toEqual([0]);
  });
  it('When input [1, 3, 5] and 2 and 4 should return []', () => {
    expect(divisibleBy([1, 3, 5], 2)).toEqual([]);
  });
  it('When input [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] and 5 should return [0, 5, 10]', () => {
    expect(divisibleBy([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 5)).toEqual([0, 5, 10]);
  });
  it('When input [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] and 9 should return [0, 9]', () => {
    expect(divisibleBy([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 9)).toEqual([0, 9]);
  });
  it('When input [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] and 10 should return [0, 10]', () => {
    expect(divisibleBy([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 10)).toEqual([0, 10]);
  });
  it('When input [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] and 3 should return [0, 3, 6, 9]', () => {
    expect(divisibleBy([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3)).toEqual([0, 3, 6, 9]);
  });
});
