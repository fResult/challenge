import { areaOrPerimeter } from './main';

describe('8 Kyu: Area or Perimeter', () => {
  it('When input 4, 4 should return 16', () => {
    expect(areaOrPerimeter(4, 4)).toBe(16);
  });
  it('When input 6, 10 should return 32', () => {
    expect(areaOrPerimeter(6, 10)).toBe(32);
  });
  it('When input 1408.3852, 577.23931 should return 32', () => {
    expect(areaOrPerimeter(1408.3852, 577.23931)).toBe(3971.24902);
  });
  it('When input 693, 693‬ should return 480249', () => {
    expect(areaOrPerimeter(693, 693)).toBe(480249);
  });
});
