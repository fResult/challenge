export const areaOrPerimeter = (length, width) => length === width ? width * length : width * 2 + length * 2;
