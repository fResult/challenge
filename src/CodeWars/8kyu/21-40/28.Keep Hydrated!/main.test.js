import { litres } from './main';

describe('8 Kyu: Keep Hydrated!', () => {
  it('When input 2 should return 1', () => {
    expect(litres(2)).toBe(1);
  });
  it('When input 1.4 should return 0', () => {
    expect(litres(1.4)).toBe(0);
  });
  it('When input 12.3 should return 6', () => {
    expect(litres(12.3)).toBe(6);
  });
  it('When input 11.8 should return 5', () => {
    expect(litres(11.8)).toBe(5);
  });
  it('When input 0.82 should return 0', () => {
    expect(litres(0.82)).toBe(0);
  });
  it('When input 1787 should return 893', () => {
    expect(litres(1787)).toBe(893);
  });
  it('When input 0 should return 0', () => {
    expect(litres(0)).toBe(0);
  });
});
