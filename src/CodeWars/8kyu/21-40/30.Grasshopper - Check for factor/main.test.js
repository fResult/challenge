import { checkForFactor } from './main';

describe('8 Kyu: Grasshopper - Check for facto', () => {
  it('When input 10, 2 should return true', () => {
    expect(checkForFactor(10, 2)).toBe(true);
  });
  it('When input 9, 2 should return false', () => {
    expect(checkForFactor(9, 2)).toBe(false);
  });
  it('When input 63, 7 should return true', () => {
    expect(checkForFactor(63, 7)).toBe(true);
  });
  it('When input 653, 7 should return false', () => {
    expect(checkForFactor(653, 7)).toBe(false);
  });
  it('When input 24612, 3 should return true', () => {
    expect(checkForFactor(24612, 3)).toBe(true);
  });
  it('When input 24617, 3 should return false', () => {
    expect(checkForFactor(24617, 3)).toBe(false);
  });
  it('When input 2450, 5 should return true', () => {
    expect(checkForFactor(2450, 5)).toBe(true);
  });
  it('When input 4287, 12 should return false', () => {
    expect(checkForFactor(4287, 12)).toBe(false);
  });
  it('When input 0, 123 should return true', () => {
    expect(checkForFactor(0, 123)).toBe(true);
  });
});
