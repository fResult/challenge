export const superSize = (num) => Number([...num.toString()].sort((a, b) => b - a).join(''));
