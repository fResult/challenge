import { superSize } from './main';

describe('noobCode 01: SUPERSIZE ME.... or rather, this integer!', () => {
  it('When input 501 should return 510', () => {
    expect(superSize(501)).toEqual(510);
  });
  it('When input 123456 should return 654321', () => {
    expect(superSize(123456)).toEqual(654321);
  });
  it('When input 12 should return 21', () => {
    expect(superSize(12)).toEqual(21);
  });
  it('When input 69 return 96', () => {
    expect(superSize(69)).toEqual(96);
  });
  it('When input 513 return 531', () => {
    expect(superSize(513)).toEqual(531);
  });
  it('When input 2017 return 7210', () => {
    expect(superSize(2017)).toEqual(7210);
  });
  it('When input 414 return 441', () => {
    expect(superSize(414)).toEqual(441);
  });
  it('When input 608719 return 987610', () => {
    expect(superSize(608719)).toEqual(987610);
  });
  it('When input 123456789 return 987654321', () => {
    expect(superSize(123456789)).toEqual(987654321);
  });
  it('When input 700000000001 return 710000000000', () => {
    expect(superSize(700000000001)).toEqual(710000000000);
  });
  it('When input 666666 return 666666', () => {
    expect(superSize(666666)).toEqual(666666);
  });
  it('When input 2 return 2', () => {
    expect(superSize(2)).toEqual(2);
  });
});
