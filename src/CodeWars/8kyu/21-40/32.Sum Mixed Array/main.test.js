import { sumMix } from "./main";

describe('8 Kyu: Sum Mixed Array', () => {
  it('When input [9, 3, \'7\', \'3\'] should return 22', () => {
    expect(sumMix([9, 3, '7', '3'])).toBe(22);
  });
  it('When input [\'5\', \'0\', 9, 3, 2, 1, \'9\', 6, 7] should return 42', () => {
    expect(sumMix(['5', '0', 9, 3, 2, 1, '9', 6, 7])).toBe(42);
  });
  it('When input [\'3\', 6, 6, 0, \'5\', 8, 5, \'6\', 2, \'0\'] should return 41', () => {
    expect(sumMix(['3', 6, 6, 0, '5', 8, 5, '6', 2, '0'])).toBe(41);
  });
  it('When input [8, 6, 3, 0, 8, 6, 8, 2, 0, 2, 4, 7] should return 54', () => {
    expect(sumMix([8, 6, 3, 0, 8, 6, 8, 2, 0, 2, 4, 7])).toBe(54);
  });
  it('When input [\'8\', \'6\', \'3\', \'0\', \'8\', \'6\', \'8\', \'2\', \'0\', \'2\', \'4\', \'7\'] should return 54', () => {
    expect(sumMix(['8', '6', '3', '0', '8', '6', '8', '2', '0', '2', '4', '7'])).toBe(54);
  });
});
