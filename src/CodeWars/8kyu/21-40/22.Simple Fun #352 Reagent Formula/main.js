export const isValid = (formula) => {
  // const mustHaveInSameTime = mat1 => mat2 => formula.includes(mat1) && formula.includes(mat2)
  //      || (!formula.includes(mat1) && !formula.includes(mat2));

  // x ↔ y is equivalent to x ⋅ y + x̄ ⋅ ȳ
  const mustHaveInSameTime = mat1 => mat2 => formula.includes(mat1) === formula.includes(mat2);
  const cannotHaveInTheSameTime = mat1 => mat2 => !(formula.includes(mat1) && formula.includes(mat2));
  const mustHaveAtLeastOne = mat1 => mat2 => !!formula.find(material => material >= mat1);

  return cannotHaveInTheSameTime(1)(2) && cannotHaveInTheSameTime(3)(4)
    && mustHaveInSameTime(5)(6) && mustHaveAtLeastOne(7)(8);
};
