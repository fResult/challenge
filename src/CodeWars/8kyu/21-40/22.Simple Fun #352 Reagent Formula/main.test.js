import { isValid } from './main';

describe('8 Kyu: Simple Fun #352: Reagent Formula', () => {
  it('When input formula [1, 3, 7] should return true', () => {
    expect(isValid([1, 3, 7])).toBe(true);
  });
  it('When input formula [7, 1, 2, 3] should return false', () => {
    expect(isValid([7, 1, 2, 3])).toBe(false);
  });
  it('When input [1, 3, 5, 7] formula should return false', () => {
    expect(isValid([1, 3, 5, 7])).toBe(false);
  });
  it('When input [1, 5, 6, 7, 3] formula should return true', () => {
    expect(isValid([1, 5, 6, 7, 3])).toBe(true);
  });
  it('When input [5, 6, 7] formula should return true', () => {
    expect(isValid([5, 6, 7])).toBe(true);
  });
  it('When input [5, 6, 7, 8] formula should return true', () => {
    expect(isValid([5, 6, 7, 8])).toBe(true);
  });
  it('When input [6, 7, 8] formula should return false', () => {
    expect(isValid([6, 7, 8])).toBe(false);
  });
  it('When input [7, 8] formula should return true', () => {
    expect(isValid([7, 8])).toBe(true);
  });
  it('When input [4, 2] formula should return false', () => {
    expect(isValid([4, 2])).toBe(false);
  });
  it('When input [1] formula should return false', () => {
    expect(isValid([1])).toBe(false);
  });
  it('When input [6, 1, 5] formula should return false', () => {
    expect(isValid([6, 1, 5])).toBe(false);
  });
  it('When input [6, 5] formula should return false', () => {
    expect(isValid([6, 5])).toBe(false);
  });
  it('When input [3, 1, 4] formula should return false', () => {
    expect(isValid([3, 1, 4])).toBe(false);
  });
  it('When input [3, 4, 7] formula should return false', () => {
    expect(isValid([3, 4, 7])).toBe(false);
  });
  it('When input [4, 8, 3, 5, 7, 2, 6] formula should return false', () => {
    expect(isValid([4, 8, 3, 5, 7, 2, 6])).toBe(false);
  });
});
