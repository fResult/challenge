import { isDivisible } from './main';

describe('8 Kyu: Is n divisible by x and y?', () => {
  it('When input 3, 3, 4 should return false', () => {
    expect(isDivisible(3, 3, 4)).toBe(false);
  });
  it('When input 12, 3, 4 should return true', () => {
    expect(isDivisible(12, 3, 4)).toBe(true);
  });
  it('When input 8, 3, 4 should return false', () => {
    expect(isDivisible(8, 3, 4)).toBe(false);
  });
  it('When input 48, 3, 4 should return true', () => {
    expect(isDivisible(12, 3, 4)).toBe(true);
  });
  it('When input 0, 3, 4 should return true', () => {
    expect(isDivisible(0, 3, 4)).toBe(true);
  });
  it('When input 12, 3, 0 should return false', () => {
    expect(isDivisible(12, 3, 0)).toBe(false);
  });
  it('When input 12, 0, 4 should return false', () => {
    expect(isDivisible(12, 0, 4)).toBe(false);
  });
});
