import { mouthSize } from './main';

describe('8 Kyu: The Wide-Mouthed frog!', () => {
  it('When input toucan should return wide', () => {
    expect(mouthSize('toucan')).toBe('wide');
  });
  it('When input alligator should return small', () => {
    expect(mouthSize('alligator')).toBe('small');
  });
  it('When input ant bear should return wide', () => {
    expect(mouthSize('ant bear')).toBe('wide');
  });
  it('When input TIGER should return wide', () => {
    expect(mouthSize('TIGER')).toBe('wide');
  });
  it('When input lion should return wide', () => {
    expect(mouthSize('lion')).toBe('wide');
  });
  it('When input Alligator should return small', () => {
    expect(mouthSize('Alligator')).toBe('small');
  });
  it('When input alli should return wide', () => {
    expect(mouthSize('alli')).toBe('wide');
  });
  it('When input ALLI should return wide', () => {
    expect(mouthSize('ALLI')).toBe('wide');
  });
  it('When input GIRAFFE should return wide', () => {
    expect(mouthSize('GIRAFFE')).toBe('wide');
  });
  it('When input AlLiGaToR should return small', () => {
    expect(mouthSize('AlLiGaToR')).toBe('small');
  });
  it('When input Alligate should return wide', () => {
    expect(mouthSize('Alligate')).toBe('wide');
  });
  it('When input Aligator should return wide', () => {
    expect(mouthSize('Aligator')).toBe('wide');
  });
});
