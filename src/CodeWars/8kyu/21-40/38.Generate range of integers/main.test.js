import { generateRange } from './main';

describe('8 Kyu: Generate range of integers', () => {
  it('When input 2, 10, 2 should return [2, 4, 6, 8, 10]', () => {
    expect(generateRange(2, 10, 2)).toEqual([2, 4, 6, 8, 10]);
  });
  it('When input 2, 10, 3 should return [2, 5, 8]', () => {
    expect(generateRange(2, 10, 3)).toEqual([2, 5, 8]);
  });
  it('When input 1, 10, 3 should return [1, 4, 7, 10]', () => {
    expect(generateRange(1, 10, 3)).toEqual([1, 4, 7, 10]);
  });
  it('When input 1, 10, 5 should return [1, 6]', () => {
    expect(generateRange(1, 10, 5)).toEqual([1, 6]);
  });
  it('When input 1, 10, 4 should return [1, 5, 9]', () => {
    expect(generateRange(1, 10, 4)).toEqual([1, 5, 9]);
  });
  it('When input 1, 10, 1 should return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]', () => {
    expect(generateRange(1, 10, 1)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  });
  it('When input 12, 120, 7 should return [13, 20, 27, 34, 41, 48, 55, 62, 69, 76, 83, 90, 97, 104, 111, 118]', () => {
    expect(generateRange(13, 120, 7)).toEqual([13, 20, 27, 34, 41, 48, 55, 62, 69, 76, 83, 90, 97, 104, 111, 118]);
  });
  it('When input 3, 120, 8 should return [3, 11, 19, 27, 35, 43, 51, 59, 67, 75, 83, 91, 99, 107, 115]', () => {
    expect(generateRange(3, 120, 8)).toEqual([3, 11, 19, 27, 35, 43, 51, 59, 67, 75, 83, 91, 99, 107, 115]);
  });
  it('When input 7, 65, 10 should return [7, 17, 27, 37, 47, 57]', () => {
    expect(generateRange(7, 65, 10)).toEqual([7, 17, 27, 37, 47, 57]);
  });
  it('When input 20, 118, 4 should return [20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116]', () => {
    expect(generateRange(20, 118, 4)).toEqual([20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116]);
  });
  it('When input 17, 7, 108 should return [17, 24, 31, 38, 45, 52, 59, 66, 73, 80, 87, 94, 101, 108]', () => {
    expect(generateRange(17, 108, 7)).toEqual([17, 24, 31, 38, 45, 52, 59, 66, 73, 80, 87, 94, 101, 108]);
  });
  it('When input 8, 7, 1 should return []', () => {
    expect(generateRange(8, 7, 1)).toEqual([]);
  });
  it('When input 8, 8, 100 should return [8]', () => {
    expect(generateRange(8, 8, 100)).toEqual([8]);
  });
});
