export const generateRange = (min, max, step) => {
  const length = Math.floor((max - min) / step) + 1;
  return new Array(length).fill().map((_, i) => min + (i * step));
};

// a₁ is a₁
// a₂ is a₁ + d
// a₃ is a₂ + d = a₁ + 2d
// a₄ is a₃ + d = a₂ + 2d = a₁ + 3d
// ...
// aₙ is aₙ₋₁ + d = a₁ + (n-1)d
