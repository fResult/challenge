const check = require('./main')

describe('8 Kyu: No Loops 2 - You only need one', () => {
  it('When input [66, 101], 66 should return true', () => {
    expect(check([66, 101], 66)).toBe(true)
  })
  it('When input [80, 117, 115, 104, 45, 85, 112, 115], 45 should return true', () => {
    expect(check([80, 117, 115, 104, 45, 85, 112, 115], 45)).toBe(true)
  })
  it("When input ['t', 'e', 's', 't'], 'e' should return true", () => {
    expect(check(['t', 'e', 's', 't'], 'e')).toBe(true)
  })
  it("When input ['what', 'a', 'great', 'kata'], 'kat', 66 should return false", () => {
    expect(check(['what', 'a', 'great', 'kata'], 'kat')).toBe(false)
  })
})
