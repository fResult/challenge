// export const check = (a, x) => !!a.find(item => item === x)
const check = (a, x) => !!a.reduce((prev, curr) => prev || (curr === x), false);

module.exports = check
