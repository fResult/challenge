export const map = (xs) => {
  const mapByTimes2 = x => x * 2;
  const result = [];
  xs.forEach(x => result.push(mapByTimes2(x)));
  return result;
};
