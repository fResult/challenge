import { map } from './main';

describe('8 Kyu: Beginner - Lost Without a Map', () => {
  it('When input [1, 2, 3] should return [4, 5, 6]', () => {
    expect(map([1, 2, 3])).toEqual([2, 4, 6])
  });
  it('When input [4, 1, 1, 1, 4] should return [8, 2, 2, 2, 8]', () => {
    expect(map([4, 1, 1, 1, 4])).toEqual([8, 2, 2, 2, 8])
  });
  it('When input [2, 2, 2, 2, 2, 2] should return [4, 4, 4, 4, 4, 4]', () => {
    expect(map([2, 2, 2, 2, 2, 2])).toEqual([4, 4, 4, 4, 4, 4])
  });
  it('When input [6, 2, 1, 7] should return [12, 4, 2, 14]', () => {
    expect(map([6, 2, 1, 7])).toEqual([12, 4, 2, 14])
  });
  it('When input [9, 2, 4, 7, 7] should return [18, 4, 8, 14, 14]', () => {
    expect(map([9, 2, 4, 7, 7])).toEqual([18, 4, 8, 14, 14])
  });
  it('When input [4, 9, 1, 4, 7, 1, 5, 7] should return [8, 18, 2, 8, 14, 2, 10, 14]', () => {
    expect(map([4, 9, 1, 4, 7, 1, 5, 7])).toEqual([8, 18, 2, 8, 14, 2, 10, 14])
  });
  it('When input [7, 1, 8, 2, 5, 9, 4] should return [14, 2, 16, 4, 10, 18, 8]', () => {
    expect(map([7, 1, 8, 2, 5, 9, 4])).toEqual([14, 2, 16, 4, 10, 18, 8])
  });
  it('When input [4, 2, 5, 3, 2, 1, 8] should return [8, 4, 10, 6, 4, 2, 16]', () => {
    expect(map([4, 2, 5, 3, 2, 1, 8])).toEqual([8, 4, 10, 6, 4, 2, 16])
  });
  it('When input [4, 3, 9, 7, 7, 9, 2, 9] should return [8, 6, 18, 14, 14, 18, 4, 18]', () => {
    expect(map([4, 3, 9, 7, 7, 9, 2, 9])).toEqual([8, 6, 18, 14, 14, 18, 4, 18])
  });
  it('When input [8, 8, 6, 5, 7, 1, 2, 2] should return [16, 16, 12, 10, 14, 2, 4, 4]', () => {
    expect(map([8, 8, 6, 5, 7, 1, 2, 2])).toEqual([16, 16, 12, 10, 14, 2, 4, 4])
  });
});
