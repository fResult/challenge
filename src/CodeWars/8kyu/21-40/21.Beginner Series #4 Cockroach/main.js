export const cockroachSpeed = (spdKmPerHr) => {
  const spdCmPerHr = spdKmPerHr * 1000 * 100;
  const spdCmPerSecond = spdCmPerHr / 60 / 60;
  return Math.round(spdCmPerSecond);
};
