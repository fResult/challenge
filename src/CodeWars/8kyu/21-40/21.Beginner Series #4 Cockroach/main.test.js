import { cockroachSpeed } from './main';

describe('8 Kyu: Beginner Series #4 Cockroach', () => {
  it('When input speed is 1.08 KM/Hr should return 30 CM/S', () => {
    expect(cockroachSpeed(1.08)).toBe(30);
  });
  it('When input speed is 1.09 KM/Hr should return 30 CM/S', () => {
    expect(cockroachSpeed(1.09)).toBe(30);
  });
  it('When input speed is 1.4074474938423567 KM/Hr should return 39 CM/S', () => {
    expect(cockroachSpeed(1.4074474938423567)).toBe(39);
  });
  it('When input speed is 0.4388663336447973 KM/Hr should return 12 CM/S', () => {
    expect(cockroachSpeed(0.4388663336447973)).toBe(12);
  });
  it('When input speed is 0.0016547904846935246 KM/Hr should return 0 CM/S', () => {
    expect(cockroachSpeed(0.0016547904846935246)).toBe(0);
  });
  it('When input speed is 2.3831839451871684 KM/Hr should return 66 CM/S', () => {
    expect(cockroachSpeed(2.3831839451871684)).toBe(66);
  });
  it('When input speed is 1.9594965642188777 KM/Hr should return 54 CM/S', () => {
    expect(cockroachSpeed(1.9594965642188777)).toBe(54);
  });
  it('When input speed is 0.832169315472115 KM/Hr should return 23 CM/S', () => {
    expect(cockroachSpeed(0.832169315472115)).toBe(23);
  });
  it('When input speed is 2.3397953482432747 KM/Hr should return 65 CM/S', () => {
    expect(cockroachSpeed(2.3397953482432747)).toBe(65);
  });
  it('When input speed is 0.17201045904386714 KM/Hr should return 5 CM/S', () => {
    expect(cockroachSpeed(0.17201045904386714)).toBe(5);
  });
  it('When input speed is 0 KM/Hr should return 0 CM/S', () => {
    expect(cockroachSpeed(0)).toBe(0);
  });
});
