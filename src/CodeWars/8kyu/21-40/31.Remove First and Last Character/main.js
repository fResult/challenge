// export const removeChar = str => str.substring(1, str.length - 1);
export const removeChar = str => str.split('').pop().shift().join('');
