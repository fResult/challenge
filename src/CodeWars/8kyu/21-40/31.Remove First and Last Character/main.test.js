import { removeChar } from './main';

describe('8 Kyu: Remove First and Last Character', () => {
  it('When input eloquent should return loquen', () => {
    expect(removeChar('eloquent')).toBe('loquen');
  });
  it('When input country should return ountr', () => {
    expect(removeChar('country')).toBe('ountr');
  });
  it('When input person should return erso', () => {
    expect(removeChar('person')).toBe('erso');
  });
  it('When input place should return lac', () => {
    expect(removeChar('place')).toBe('lac');
  });
  it('When input d11b1a33a should return 11b1a33', () => {
    expect(removeChar('d11b1a33a')).toBe('11b1a33');
  });
  it('When input Sila Setthakan-anan should return ila Setthakan-ana', () => {
    expect(removeChar('Sila Setthakan-anan')).toBe('ila Setthakan-ana');
  });
});
