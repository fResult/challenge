import { reverseSeq } from './main';

describe('8 Kyu: Reversed sequence', () => {
  it('When input 5 should return [5, 4, 3, 2, 1]', () => {
    expect(reverseSeq(5)).toEqual([5, 4, 3, 2, 1]);
  });
  it('When input 6 should return [6, 5, 4, 3, 2, 1]', () => {
    expect(reverseSeq(6)).toEqual([6, 5, 4, 3, 2, 1]);
  });
  it('When input 3 should return [3, 2, 1]', () => {
    expect(reverseSeq(3)).toEqual([3, 2, 1]);
  });
  it('When input 2 should return [2, 1]', () => {
    expect(reverseSeq(2)).toEqual([2, 1]);
  });
  it('When input 1 should return [1]', () => {
    expect(reverseSeq(1)).toEqual([1]);
  });
  it('When input 0 should return []', () => {
    expect(reverseSeq(0)).toEqual([]);
  });
});
