export const reverseSeq = n => {
  const arr = [];
  while(n > 0) arr.push(n--);
  return arr;
};
