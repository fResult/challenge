import { Kata } from './main';

describe('8 Kyu: `Volume of a Cuboid', () => {
  it('WHen input 1, 2, 2 should return 4', () => {
    expect(Kata.getVolumeOfCuboid(1, 2, 2)).toEqual(4);
  });
  it('WHen input 6.3, 2, 5 should return 63', () => {
    expect(Kata.getVolumeOfCuboid(6.3, 2, 5)).toEqual(63);
  });
  it('WHen input 4, 3, 5 should return 60', () => {
    expect(Kata.getVolumeOfCuboid(4, 3, 5)).toEqual(60);
  });
  it('WHen input 5, 5, 0 should return 0', () => {
    expect(Kata.getVolumeOfCuboid(5, 5, 0)).toEqual(0);
  });
  it('WHen input 544, 333, 142 should return 25723584‬', () => {
    expect(Kata.getVolumeOfCuboid(544, 333, 142)).toEqual(25723584);
  });
});
