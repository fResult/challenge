8 Kyu: Convert number to reversed array of digits
==
Description:
--
#### Given a random number:
* **C#:** long;
* **C++:** unsigned long;  

You have to return the digits of this number within an array in reverse order.

### Example:
```text
348597 => [7,9,5,8,4,3]
```

Source:
--
https://www.codewars.com/kata/5583090cbe83f4fd8c000051/train/typescript
