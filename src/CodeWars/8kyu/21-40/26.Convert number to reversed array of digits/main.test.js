import { digitize } from './main';

describe('8 Kyu: Convert number to reversed array of digits', () => {
  it('When input 35231 should return [1, 3, 2, 5, 3]', () => {
    expect(digitize(35231)).toEqual([1, 3, 2, 5, 3]);
  });
  it('When input 9876543210 should return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
    expect(digitize(9876543210)).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });
  it('When input 123456789 should return [9, 8, 7, 6, 5, 4, 3, 2, 1]', () => {
    expect(digitize(123456789)).toEqual([9, 8, 7, 6, 5, 4, 3, 2, 1]);
  });
  it('When input 23582357 should return [7, 5, 3, 2, 8, 5, 3, 2]', () => {
    expect(digitize(23582357)).toEqual([7, 5, 3, 2, 8, 5, 3, 2]);
  });
  it('When input 14 should return [4, 1]', () => {
    expect(digitize(14)).toEqual([4, 1]);
  });
  it('When input 9 should return [9]', () => {
    expect(digitize(9)).toEqual([9]);
  });
  it('When input 0 should return [0]', () => {
    expect(digitize(0)).toEqual([0]);
  });
});
