export const digitize = (num) => num.toString().split('').reverse().map(Number);
