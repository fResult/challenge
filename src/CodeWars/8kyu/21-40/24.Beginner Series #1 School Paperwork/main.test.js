import { paperwork } from './main';

describe('8 Kyu: Beginner Series #1 School Paperwork', () => {
  it('When input 5, 5 should return 25', () => {
    expect(paperwork(5, 5)).toBe(25);
  });
  it('When input 783, 908 should return 710964', () => {
    expect(paperwork(783, 908)).toBe(710964);
  });
  it('When input 18, 882 should return 15876', () => {
    expect(paperwork(18, 882)).toBe(15876);
  });
  it('When input 5, -5 should return 0', () => {
    expect(paperwork(5, -5)).toBe(0);
  });
  it('When input -5, 5 should return 0', () => {
    expect(paperwork(-5, 5)).toBe(0);
  });
  it('When input -5, -5 should return 0', () => {
    expect(paperwork(-5, -5)).toBe(0);
  });
});
