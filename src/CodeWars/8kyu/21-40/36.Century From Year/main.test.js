import { centuryFromYear } from './main';

describe('8 Kyu: Century From Year', () => {
  it('When input 1705 should return 18', () => {
    expect(centuryFromYear(1705)).toBe(18);
  });
  it('When input 1900 should return 19', () => {
    expect(centuryFromYear(1900)).toBe(19);
  });
  it('When input 1601 should return 17', () => {
    expect(centuryFromYear(1601)).toBe(17);
  });
  it('When input 2000 should return 20', () => {
    expect(centuryFromYear(2000)).toBe(20);
  });
  it('When input 2020 should return 21', () => {
    expect(centuryFromYear(2020)).toBe(21);
  });
  it('When input 2120 should return 22', () => {
    expect(centuryFromYear(2120)).toBe(22);
  });
  it('When input 824390 should return 8244', () => {
    expect(centuryFromYear(824390)).toBe(8244);
  });
  it('When input 824400 should return 8244', () => {
    expect(centuryFromYear(824400)).toBe(8244);
  });
  it('When input 1000000 should return 10000', () => {
    expect(centuryFromYear(1000000)).toBe(10000);
  });
});
