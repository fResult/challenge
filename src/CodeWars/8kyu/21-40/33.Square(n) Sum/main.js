export const squareSum = numbers => numbers.reduce((sum, number) => number ** 2 + sum, 0);
