import { squareSum } from './main';

describe('8 Kyu: Square(n) Sum', () => {
  it('When input [1, 2] should return 5', () => {
    expect(squareSum([1, 2])).toBe(5);
  });
  it('When input [0, 3, 4, 5] should return 50', () => {
    expect(squareSum([0, 3, 4, 5])).toBe(50);
  });
  it('When input [0, 0, 0, 5] should return 25', () => {
    expect(squareSum([0, 0, 0, 5])).toBe(25);
  });
  it('When input [0, 0, 0, 0] should return 0', () => {
    expect(squareSum([0, 0, 0, 0])).toBe(0);
  });
  it('When input [2, 3, 4] should return 29', () => {
    expect(squareSum([2, 3, 4])).toBe(29);
  });
});
