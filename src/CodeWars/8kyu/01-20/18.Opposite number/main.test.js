import { opposite } from './main';

describe('8 Kyu: Opposite number', () => {
  it('When input 1 should return -1', () => {
    expect(opposite(1)).toBe(-1);
  });
  it('When input 2 should return -2', () => {
    expect(opposite(2)).toBe(-2);
  });
  it('When input 12525220.3325 should return -12525220.3325', () => {
    expect(opposite(12525220.3325)).toBe(-12525220.3325);
  });
  it('When input -1 should return 1', () => {
    expect(opposite(-1)).toBe(1);
  });
  it('When input -2 should return 2', () => {
    expect(opposite(-2)).toBe(2);
  });
  it('When input -12525220.3325 should return 12525220.3325', () => {
    expect(opposite(-12525220.3325)).toBe(12525220.3325);
  });
  it('When input 0 should return 0', () => {
    expect(opposite(0)).toBe(0);
  });
});
