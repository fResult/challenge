8 Kyu: Opposite number
==
Description:
--
Very simple, given a number, find its opposite.

#### Examples:
```textmate
  1 => -1
 14 => -14
-34 =>  34
```

Source:
--
https://www.codewars.com/kata/56dec885c54a926dcd001095/train/typescript
