import { divide } from "./main";

describe('8 Kyu: Watermelon', () => {
  it('When input 4 should return true', () => {
    expect(divide(4)).toEqual(true)
  });
  it('When input 2 should return false', () => {
    expect(divide(2)).toEqual(false)
  });
  it('When input 5 should return false', () => {
    expect(divide(5)).toEqual(false)
  });
  it('When input 88 should return true', () => {
    expect(divide(88)).toEqual(true)
  });
  it('When input 100 should return true', () => {
    expect(divide(100)).toEqual(true)
  });
  it('When input 67 should return false', () => {
    expect(divide(67)).toEqual(false)
  });
  it('When input 90 should return true', () => {
    expect(divide(90)).toEqual(true)
  });
  it('When input 10 should return true', () => {
    expect(divide(10)).toEqual(true)
  });
  it('When input 99 should return false', () => {
    expect(divide(99)).toEqual(false)
  });
  it('When input 32 should return true', () => {
    expect(divide(32)).toEqual(true)
  });
});
