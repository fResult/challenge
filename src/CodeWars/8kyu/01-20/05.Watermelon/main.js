export const divide = amount => amount > 2 && amount % 2 === 0;
