export const shark = (pontoonDistance, sharkDistance, youSpeed, sharkSpeed, dolphin) => {
  const sharkActualSpeed = dolphin ? sharkSpeed / 2 : sharkSpeed;

  const youTime = pontoonDistance / youSpeed;
  const sharkTime = sharkDistance / sharkActualSpeed;

  const canSharkEatYou = youTime < sharkTime;

  return canSharkEatYou ? 'Alive!' : 'Shark Bait!'
};
