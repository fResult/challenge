import { shark } from './main';

describe('8 Kyu: Holiday VI - Shark Pontoon', () => {
  it('When input 12, 50, 4, 8, true should return Alive!', () => {
    expect(shark(12, 50, 4, 8, true)).toBe('Alive!');
  });
  it('When input 7, 55, 4, 16, true should return Alive!', () => {
    expect(shark(7, 55, 4, 16, true)).toBe('Alive!');
  });
  it('When input 24, 0, 4, 8, true should return Shark Bait!', () => {
    expect(shark(24, 0, 4, 8, true)).toBe('Shark Bait!');
  });
  it('When input 32, 189, 1, 13, true should return Shark Bait!', () => {
    expect(shark(32, 189, 1, 13, true)).toBe('Shark Bait!');
  });
  it('When input 43, 99, 1, 5, false should return Shark Bait!', () => {
    expect(shark(43, 99, 1, 5, false)).toBe('Shark Bait!');
  });
  it('When input 41, 176, 0, 14, true should return Shark Bait!', () => {
    expect(shark(41, 176, 0, 14, true)).toBe('Shark Bait!');
  });
  it('When input 14, 188, 4, 9, false should return Alive!', () => {
    expect(shark(14, 188, 4, 9, false)).toBe('Alive!');
  });
  it('When input 28, 199, 0, 19 true should return Shark Bait!', () => {
    expect(shark(28, 199, 0, 19, true)).toBe('Shark Bait!');
  });
  it('When input 1, 163, 4, 7, true should return Alive!', () => {
    expect(shark(1, 163, 4, 7, true)).toBe('Alive!');
  });
  it('When input 21, 20, 1, 4, false should return Shark Bait!', () => {
    expect(shark(21, 20, 1, 4, false)).toBe('Shark Bait!');
  });
  it('When input 14, 110, 1, 6, false should return Alive!', () => {
    expect(shark(14, 110, 1, 6, false)).toBe('Alive!');
  });
  it('When input 23, 162, 2, 9, true should return Alive!', () => {
    expect(shark(23, 162, 2, 9, true)).toBe('Alive!');
  });
});
