import { goals } from './main';

describe('8 Kyu: Grasshopper - Messi goals function', () => {
  it('When input 13, 15 and 30 should return 58', () => {
    expect(goals(13, 15, 30)).toBe(58);
  });
  it('When input 24, 26 and 20 should return 72', () => {
    expect(goals(24, 26, 22)).toBe(72);
  });
  it('When input 0, 0 and 0 should return 0', () => {
    expect(goals(0, 0, 0)).toBe(0);
  });
  it('When input 100, 100 and 100 should return 300', () => {
    expect(goals(100, 100, 100)).toBe(300);
  });
});
