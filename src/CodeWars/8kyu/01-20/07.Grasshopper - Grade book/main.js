export const getGrade = (a, b, c) => {
  const averageScore = (a + b + c) / 3;
  return averageScore >= 90 ? 'A' :
    averageScore >= 80 ? 'B' :
      averageScore >= 70 ? 'C' :
        averageScore >= 60 ? 'D' :
          'F';
};

function calculateGrade(score) {
  if (score >= 76) {
    return 'Satisfactory'
  } else if (score >= 60) {
    return 'Satisfactory'
  } else {
    return 'Unsatisfactory'
  }
}
