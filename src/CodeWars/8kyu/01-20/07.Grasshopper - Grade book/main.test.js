import { getGrade } from './main';

describe('8 Kyu: Grasshopper - Grade book', () => {
  it('When input scores 95, 90 and 93 should return grade A', () => {
    expect(getGrade(95, 90, 93)).toEqual('A');
  });
  it('When input scores 70, 70 and 100 should return grade B', () => {
    expect(getGrade(70, 70, 100)).toEqual('B');
  });
  it('When input scores 70, 70 and 70 should return grade C', () => {
    expect(getGrade(70, 70, 70)).toEqual('C');
  });
  it('When input scores 65, 70 and 59 should return grade D', () => {
    expect(getGrade(65, 70, 59)).toEqual('D');
  });
  it('When input scores 44, 55 and 52 should return grade F', () => {
    expect(getGrade(44, 55, 52)).toEqual('F');
  });
  it('When input scores 100, 85 and 96 should return grade A', () => {
    expect(getGrade(100, 85, 96)).toEqual('A');
  });
  it('When input scores 82, 85 and 87 should return grade B', () => {
    expect(getGrade(82, 85, 87)).toEqual('B');
  });
  it('When input scores 75, 70 and 79 should return grade C', () => {
    expect(getGrade(75, 70, 79)).toEqual('C');
  });
  it('When input scores 66, 62 and 68 should return grade D', () => {
    expect(getGrade(66, 62, 68)).toEqual('D')
  });
  it('When input scores 48, 55 and 52 should return grade F', () => {
    expect(getGrade(48, 55, 52)).toEqual('F')
  });
  it('When input scores 100, 100 and 100 should return grade A', () => {
    expect(getGrade(100, 100, 100)).toEqual('A');
  });
  it('When input scores 84, 79, 85 should return grade B', () => {
    expect(getGrade(84, 79, 85)).toEqual('B');
  });
  it('When input scores 60, 82, 76 should return grade C', () => {
    expect(getGrade(60, 82, 76)).toEqual('C');
  });
  it('When input scores 58, 62, 70 should return grade D', () => {
    expect(getGrade(58, 62, 70)).toEqual('D')
  });
  it('When input scores 0, 0 and 0 should return grade F', () => {
    expect(getGrade(0, 0, 0)).toEqual('F')
  });
});
