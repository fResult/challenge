import { past } from './main';

describe('8 Kyu: Beginner Series #2 Clock', () => {
  it('When input 0 hour(s) 1 minute(s) and 1 second(s) should return 61000 milliseconds', () => {
    expect(past(0, 1, 1)).toEqual(61000);
  });
  it('When input 1 hour(s) 1 minute(s) and 1 second(s) should return 3661000 milliseconds', () => {
    expect(past(1, 1, 1)).toEqual(3661000);
  });
  it('When input 0 hour(s) 0 minute(s) and 0 second(s) should return 0 milliseconds', () => {
    expect(past(0, 0, 0)).toEqual(0);
  });
  it('When input 1 hour(s) 0 minute(s) and 1 second(s) should return 3601000 milliseconds', () => {
    expect(past(1, 0, 1)).toEqual(3601000);
  });
  it('When input 1 hour(s) 0 minute(s) and 1 second(s) should return 3601000 milliseconds', () => {
    expect(past(1, 0, 1)).toEqual(3601000);
  });
  it('When input 1 hour(s) 0 minute(s) and 0 second(s) should return 3600000 milliseconds', () => {
    expect(past(1, 0, 0)).toEqual(3600000);
  });
  it('When input 3 hour(s) 45 minute(s) and 59 second(s) should return 13559000 milliseconds', () => {
    expect(past(3, 45, 59)).toEqual(13559000);
  });
});
