const convertSecondsToMilliseconds = s => 1000 * s
const convertMinutesToMilliseconds = m => 1000 * 60 * m
const convertHoursToMilliseconds = h => 1000 * 60 * 60 * h
export const past = (h, m, s) => {
  return convertHoursToMilliseconds(h) + convertMinutesToMilliseconds(m) + convertSecondsToMilliseconds(s)
}
