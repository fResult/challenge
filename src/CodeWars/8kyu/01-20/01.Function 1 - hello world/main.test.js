import { greet } from './main'

describe('8 Kyu: Function 1: hello world', () => {
  it('greet should be a function', () => {
    expect(typeof greet).toBe('function');
  });
  it('greet should return \'hello world!\'', () => {
    expect(greet()).toEqual('hello world!');
  });
});
