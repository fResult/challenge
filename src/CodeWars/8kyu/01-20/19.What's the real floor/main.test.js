import { getRealFloor } from './main';

describe('8Kyu: What\'s the real floor', () => {
  it('When input floor 1 should get real floor is 0', () => {
    expect(getRealFloor(1)).toBe(0);
  });
  it('When input floor 0 should get real floor is 0', () => {
    expect(getRealFloor(0)).toBe(0);
  });
  it('When input floor 5 should get real floor is 4', () => {
    expect(getRealFloor(5)).toBe(4);
  });
  it('When input floor 10 should get real floor is 9', () => {
    expect(getRealFloor(10)).toBe(9);
  });
  it('When input floor 15 should get real floor is 13', () => {
    expect(getRealFloor(15)).toBe(13);
  });
  it('When input floor -3 should get real floor is -3', () => {
    expect(getRealFloor(-3)).toBe(-3);
  });
  it('When input floor -145 should get real floor is -145', () => {
    expect(getRealFloor(-145)).toBe(-145);
  });
  it('When input floor 198 should get real floor is 196', () => {
    expect(getRealFloor(198)).toBe(196);
  });
});
