export const getRealFloor = num => num > 13 ? num - 2 : num > 0 ? num - 1 : num;
