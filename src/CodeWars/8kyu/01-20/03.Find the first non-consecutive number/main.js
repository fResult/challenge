const findFirstNonConsecutive = (arr) => {
  const result = arr.find((item, idx) => item > arr[idx - 1] + 1 ? item : null)
  return result !== undefined ? result : null
}

module.exports = findFirstNonConsecutive
