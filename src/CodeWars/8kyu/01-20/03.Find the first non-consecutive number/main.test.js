// import { findFirstNonConsecutive } from './main'
const findFirstNonConsecutive = require('./main')
// import { describe, it, expect } from '@jest/globals'
const { describe, it, expect } = require('@jest/globals')


console.log(findFirstNonConsecutive([1, 2, 3, 4, 6, 8]))

describe('Find the first non-consecutive number', () => {
  it('When input [1, 2, 3, 4, 5, 6, 7, 8] then return null', () => {
    expect(findFirstNonConsecutive([1, 2, 3, 4, 5, 6, 7, 8])).toEqual(null)
  })
  it('When input [1, 2, 3, 4, 6, 7, 8] then return 6', () => {
    expect(findFirstNonConsecutive([1, 2, 3, 4, 6, 7, 8])).toEqual(6)
  })
  it('When input [8, 10, 12, 13] then return 10', () => {
    expect(findFirstNonConsecutive([8, 10, 12, 13])).toEqual(10)
  })
  it('When input [8, 9, 10, 13, 18] then return 13', () => {
    expect(findFirstNonConsecutive([8, 9, 10, 13, 18])).toEqual(13)
  })
  it('When input [3, 4, 5, 6, 100] then return 100', () => {
    expect(findFirstNonConsecutive([3, 4, 5, 6, 100])).toEqual(100)
  })
  it('When input [3, 4, 6, 100, 101] then return 6', () => {
    expect(findFirstNonConsecutive([3, 4, 6, 100, 101])).toEqual(6)
  })
  it('When input [100, 101, 102, 103, 104, 105, 106, 107, 108, 109] then return null', () => {
    expect(findFirstNonConsecutive([100, 101, 102, 103, 104, 105, 106, 107, 108, 109])).toEqual(null)
  })
  it('When input [8] then return null', () => {
    expect(findFirstNonConsecutive([8])).toEqual(null)
  })
  it('When input [] then return null', () => {
    expect(findFirstNonConsecutive([])).toEqual(null)
  })
})
