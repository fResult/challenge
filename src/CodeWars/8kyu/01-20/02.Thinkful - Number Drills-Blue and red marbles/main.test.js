import { guessBlue } from './main'

describe('8 Kyu: Thinkful - Number Drills: Blue and red marbles', () => {
  it('Probability when put blue 5 red 5 and pulled blue 2 red 3 will be 0.6', () => {
    expect(guessBlue(5, 5, 2, 3)).toEqual(0.6);
  });
  it('Probability when put blue 5 red 7 and pulled blue 4 red 3 will be 0.2', () => {
    expect(guessBlue(5, 7, 4, 3)).toEqual(0.2);
  });
  it('Probability when put blue 12 red 18 and pulled blue 4 red 6 will be 0.4', () => {
    expect(guessBlue(12, 18, 4, 6)).toEqual(0.4);
  });
  it('Probability when put blue 20 red 32 and pulled blue 18 red 14 will be 0.1', () => {
    expect(guessBlue(20, 32, 18, 14)).toEqual(0.1);
  });
});
