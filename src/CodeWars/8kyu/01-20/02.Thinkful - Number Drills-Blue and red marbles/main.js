export const guessBlue = (blueStart, redStart, bluePulled, redPulled) => {
  const blueRemain = blueStart - bluePulled;
  const redRemain = redStart - redPulled;
  // Probability event of guessBlue -> P(E) = n/N
  return blueRemain / (redRemain + blueRemain); // n/N = amountOfBlue / amountOfAll
};
