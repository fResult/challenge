import { findLongest } from './main';

describe('8 Kyu: Squash the bugs', () => {
  it('When Input "The quick white fox jumped around the massive dog" Should return 7', () => {
    expect(findLongest('The quick white fox jumped around the massive dog')).toBe(7);
  });
  it('When Input "Take me to tinseltown with you" Should return 10', () => {
    expect(findLongest('Take me to tinseltown with you')).toBe(10);
  });
  it('When Input "Sausage chops" Should return 7', () => {
    expect(findLongest('Sausage chops')).toBe(7);
  });
  it('When Input "Wind your body and wiggle your belly" Should return 6', () => {
    expect(findLongest('Wind your body and wiggle your belly')).toBe(6);
  });
  it('When Input "Lets all go on holiday" Should return 7', () => {
    expect(findLongest('Lets all go on holiday')).toBe(7);
  });
});
