export const findLongest = str => Math.max(...str.split(' ').map(word => word.length));
