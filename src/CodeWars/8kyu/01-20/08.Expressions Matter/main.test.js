import { expressionsMatter } from './main';

describe('8 Kyu: Expressions Matter', () => {
  it('When input 2, 1, 2 should return max is 6', () => {
    expect(expressionsMatter(2, 1, 2)).toEqual(6);
  });
  it('When input 2, 1, 1 should return max is 4', () => {
    expect(expressionsMatter(2, 1, 1)).toEqual(4);
  });
  it('When input 1, 1, 1 should return max is 3', () => {
    expect(expressionsMatter(1, 1, 1)).toEqual(3);
  });
  it('When input 1, 2, 3 should return max is 9', () => {
    expect(expressionsMatter(1, 2, 3)).toEqual(9);
  });
  it('When input 1, 3, 1 should return max is 5', () => {
    expect(expressionsMatter(1, 3, 1)).toEqual(5);
  });
  it('When input 2, 2, 2 should return max is 8', () => {
    expect(expressionsMatter(2, 2, 2)).toEqual(8);
  });
  it('When input 5, 1, 3 should return max is 20', () => {
    expect(expressionsMatter(5, 1, 3)).toEqual(20);
  });
  it('When input 3, 5, 7 should return max is 105', () => {
    expect(expressionsMatter(3, 5, 7)).toEqual(105);
  });
  it('When input 5, 6, 1 should return max is 35', () => {
    expect(expressionsMatter(5, 6, 1)).toEqual(35);
  });
  it('When input 1, 6, 1 should return max is 8', () => {
    expect(expressionsMatter(1, 6, 1)).toEqual(8);
  });
  it('When input 2, 6, 1 should return max is 14', () => {
    expect(expressionsMatter(2, 6, 1)).toEqual(14);
  });
  it('When input 6, 7, 1 should return max is 48', () => {
    expect(expressionsMatter(6, 7, 1)).toEqual(48);
  });
  it('When input 2, 10, 3 should return max is 60', () => {
    expect(expressionsMatter(2, 10, 3)).toEqual(60);
  });
  it('When input 1, 8, 3 should return max is 27', () => {
    expect(expressionsMatter(1, 8, 3)).toEqual(27);
  });
  it('When input 9, 7, 2 should return max is 126', () => {
    expect(expressionsMatter(9, 7, 2)).toEqual(126);
  });
  it('When input 1, 1, 10 should return max is 20', () => {
    expect(expressionsMatter(1, 1, 10)).toEqual(20);
  });
  it('When input 9, 1, 1 should return max is 18', () => {
    expect(expressionsMatter(9, 1, 1)).toEqual(18);
  });
  it('When input 10, 5, 6 should return max is 300', () => {
    expect(expressionsMatter(10, 5, 6)).toEqual(300);
  });
  it('When input 1, 10, 1 should return max is 12', () => {
    expect(expressionsMatter(1, 10, 1)).toEqual(12);
  });
});
