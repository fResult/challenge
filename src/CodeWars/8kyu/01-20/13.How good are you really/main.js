export const betterThanAverage = (classPoints, yourPoint) => yourPoint > (classPoints.reduce((acc, num) => acc + num, 0) / classPoints.length);
