import { betterThanAverage } from './main';

describe('8 Kyu: How good are you really?', () => {
  it('When input classPoint is [2, 3] and yourPoint is 5 should return True', () => {
    expect(betterThanAverage([2, 3], 5)).toBeTrue();
  });
  it('When input classPoint is [100, 40, 34, 57, 29, 72, 57, 88] and yourPoint is 75 should return True', () => {
    expect(betterThanAverage([100, 40, 34, 57, 29, 72, 57, 88], 75)).toBeTrue();
  });
  it('When input classPoint is [12, 23, 34, 45, 56, 67, 78, 89, 90] and yourPoint is 69 should return True', () => {
    expect(betterThanAverage([12, 23, 34, 45, 56, 67, 78, 89, 90], 69)).toBeTrue();
  });
  it('When input classPoint is [5, 5, 5, 6] and yourPoint is 5 should return False', () => {
    expect(betterThanAverage([5, 5, 5, 6], 5)).toBeFalse();
  });
  it('When input classPoint is [1, 2, 3, 4, 5] and yourPoint is 3.1 should return True', () => {
    expect(betterThanAverage([1, 2, 3, 4, 5], 3.1)).toBeTrue();
  });
  it('When input classPoint is [1, 2, 3, 4, 5] and yourPoint is 3 should return False', () => {
    expect(betterThanAverage([1, 2, 3, 4, 5], 3)).toBeFalse();
  });
  it('When input classPoint is [10, 10, 10, 10, 10] and yourPoint is 11 should return True', () => {
    expect(betterThanAverage([10, 10, 10, 10, 10], 11)).toBeTrue();
  });
});
