import { isZeroFuel } from "./main";

describe('8 Kyu: Will you make it?', () => {
  it('When input distance 50, mpg 25 and fuelLeft 2 should return true', () => {
    expect(isZeroFuel(50, 25, 2)).toEqual(true);
  });
  it('When input distance 100, mpg 50 and fuelLeft 1 should return false', () => {
    expect(isZeroFuel(100, 50, 1)).toEqual(false);
  });
  it('When input distance 150, mpg 60 and fuelLeft 3 should return true', () => {
    expect(isZeroFuel(150, 60, 3)).toEqual(true);
  });
  it('When input distance 150, mpg 60 and fuelLeft 3 should return true', () => {
    expect(isZeroFuel(150, 60, 3)).toEqual(true);
  });
  it('When input distance 200, mpg 99 and fuelLeft 2 should return false', () => {
    expect(isZeroFuel(200, 99, 2)).toEqual(false);
  });
  it('When input distance 20, mpg 7 and fuelLeft 3 should return true', () => {
    expect(isZeroFuel(20, 7, 3)).toEqual(true);
  });
  it('When input distance 5, mpg 0 and fuelLeft 10 should return true', () => {
    expect(isZeroFuel(5, 0.5, 10)).toEqual(true);
  });
  it('When input distance 5, mpg 0 and fuelLeft 100 should return false', () => {
    expect(isZeroFuel(5, 0, 100)).toEqual(false);
  });
});
