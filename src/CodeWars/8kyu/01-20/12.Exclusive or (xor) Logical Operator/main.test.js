import { xor } from './main';

describe('8 Kyu: Exclusive "or" (xor) Logical Operator', () => {
  it('When False xor with False should return False', () => {
    expect(xor(false, false)).toEqual(false);
  });
  it('When True xor with False should return True', () => {
    expect(xor(true, false)).toEqual(true);
  });
  it('When False xor with True should return True', () => {
    expect(xor(false, true)).toEqual(true);
  });
  it('When True xor with True should return False', () => {
    expect(xor(true, true)).toEqual(false);
  });
  it('When True xor with True shouldn\'t return False', () => {
    expect(xor(true, true)).not.toEqual(true);
  });
});
