import { arr } from './main';

describe('8 Kyu: Filling an array (part 1)', () => {
  it('arr() should be instance of Array', () => {
    expect(arr() instanceof Array).toBeTrue();
  });
  it('When have no input should return Empty array', () => {
    expect(arr()).toEqual([]);
  });
  it('When input 4 should return [0, 1, 2, 3]', () => {
    expect(arr(4)).toEqual([0, 1, 2, 3]);
  });
  it('When input 5 should return [0, 1, 2, 3, 4]', () => {
    expect(arr(5)).toEqual([0, 1, 2, 3, 4]);
  });
  it('When input 6 should return [0, 1, 2, 3, 4, 5]', () => {
    expect(arr(6)).toEqual([0, 1, 2, 3, 4, 5]);
  });
  it('When input 15 should return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]', () => {
    expect(arr(15)).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]);
  });
});
