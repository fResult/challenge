import { summation } from './main';

describe('8 Kyu: Grasshopper - Summation', () => {
  it('When input num is 8 should return 36', () => {
    expect(summation(8)).toEqual(36);
  });
  it('When input num is 1 should return 1', () => {
    expect(summation(1)).toEqual(1);
  });
  it('When input num is 10 should return 55', () => {
    expect(summation(10)).toEqual(55);
  });
  it('When input num is 100 should return 5050', () => {
    expect(summation(100)).toEqual(5050);
  });
});
