export const arrayPlusArray = (arr1, arr2) => arr1.concat(arr2).reduce((acc, num) => acc + num, 0);
