import { arrayPlusArray } from './main1';

describe('8 Kyu: Array plus array', () => {
  it('When input [1, 2, 3] and [4, 5, 6] should return 21', () => {
    expect(arrayPlusArray([1, 2, 3], [4, 5, 6])).toBe(21);
  });
  it('When input [-1, -2, -3] and [-4, -5, -6] should return -21', () => {
    expect(arrayPlusArray([-1, -2, -3], [-4, -5, -6])).toBe(-21);
  });
  it('When input [0, 0, 0] and [4, 5, 6] should return 15', () => {
    expect(arrayPlusArray([0, 0, 0], [4, 5, 6])).toBe(15);
  });
  it('When input [100, 200, 300] and [400, 500, 600] should return 2100', () => {
    expect(arrayPlusArray([100, 200, 300], [400, 500, 600])).toBe(2100);
  });
  it('When input [-9768,-9104,6613,-4637,-1316] and [ 9275,-6418,8594,-2772,4974] should return -4559', () => {
    expect(arrayPlusArray([-9768, -9104, 6613, -4637, -1316], [9275, -6418, 8594, -2772, 4974])).toBe(-4559);
  });
  it('When input [2906, 1261, 6197, -7724, -478] and [6461, -9466, 1633, 4187, 5446] should return 10423', () => {
    expect(arrayPlusArray([2906, 1261, 6197, -7724, -478], [6461, -9466, 1633, 4187, 5446])).toBe(10423);
  });
  it('When input [9069, 4531, 7879, 2799, 5051] and [5431, 4155, -4460, -1648, -4856] should return 27951', () => {
    expect(arrayPlusArray([9069, 4531, 7879, 2799, 5051], [5431, 4155, -4460, -1648, -4856])).toBe(27951);
  });
  it('When input [-5105, 2974, 1654, -2580, 17] and [285, 748, -9791, -7132, -1085] should return -20015', () => {
    expect(arrayPlusArray([-5105, 2974, 1654, -2580, 17], [285, 748, -9791, -7132, -1085])).toBe(-20015);
  });
});
