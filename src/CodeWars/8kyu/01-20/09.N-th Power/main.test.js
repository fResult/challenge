import { index } from './main';

describe('8 Kyu: N-th Power', () => {
  it('When input [1, 2, 3, 4] and 2 should return 9', () => {
    expect(index([1, 2, 3, 4], 2)).toEqual(9)
  });
  it('When input [1, 3, 10, 100] and 3 should return 1000000', () => {
    expect(index([1, 3, 10, 100], 3)).toEqual(1000000)
  });
  it('When input [0, 1] and 0 should return 1', () => {
    expect(index([0, 1], 0)).toEqual(1)
  });
  it('When input [1, 2] and 3 should return -1', () => {
    expect(index([1, 2], 3)).toEqual(-1)
  });
  it('When input [0] and 0 should return 1', () => {
    expect(index([0], 0)).toEqual(1)
  });
  it('When input [1,1,1,1,1,1,1,1,1,1] and 9 should return 1', () => {
    expect(index([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 9)).toEqual(1)
  });
  it('When input [1,1,1,1,1,1,1,1,1,2] and 9 should return 512', () => {
    expect(index([1, 1, 1, 1, 1, 1, 1, 1, 1, 2], 9)).toEqual(512)
  });
  it('When input [29,82,45,10] and 3 should return 1000', () => {
    expect(index([29, 82, 45, 10], 3)).toEqual(1000)
  });
  it('When input [6,31] and 3 should return -1', () => {
    expect(index([6, 31], 3)).toEqual(-1)
  });
  it('When input [75,68,35,61,9,36,89,0,30] and 10 should return -1', () => {
    expect(index([75,68,35,61,9,36,89,0,30], 10)).toEqual(-1)
  });
});
