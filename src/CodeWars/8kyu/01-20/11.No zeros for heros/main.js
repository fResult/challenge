const noBoringZeros = num => (num === 0 || num % 10 !== 0) ? num : noBoringZeros(num / 10)

module.exports = noBoringZeros
