const noBoringZeros = require('./main')

describe('8 Kyu: No zeros for heroes', () => {
  it('When input 1450 should return 145', () => {
    expect(noBoringZeros(1450)).toEqual(145)
  })
  it('When input 960000 should return 96', () => {
    expect(noBoringZeros(960000)).toEqual(96)
  })
  it('When input 1050 should return 105', () => {
    expect(noBoringZeros(1050)).toEqual(105)
  })
  it('When input -1050 should return -105', () => {
    expect(noBoringZeros(-1050)).toEqual(-105)
  })
  it('When input -105 should return -105', () => {
    expect(noBoringZeros(-105)).toEqual(-105)
  })
  it('When input 0 should return 0', () => {
    expect(noBoringZeros(0)).toEqual(0)
  })
})
