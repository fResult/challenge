import { hero } from './main';

describe('8 Kyu: Is he gonna survive?', () => {
  it('When hero has 10 bullets and the castle has 5 dragon, should return true', () => {
    expect(hero(10, 5)).toBeTrue();
  });
  it('When hero has 7 bullets and the castle has 4 dragon, should return false', () => {
    expect(hero(7, 4)).toBeFalse();
  });
  it('When hero has 100 bullets and the castle has 40 dragon, should return true', () => {
    expect(hero(100, 40)).toBeTrue();
  });
  it('When hero has 1500 bullets and the castle has 751 dragon, should return false', () => {
    expect(hero(1500, 751)).toBeFalse();
  });
  it('When hero has 0 bullets and the castle has 1 dragon, should return false', () => {
    expect(hero(0, 1)).toBeFalse();
  });
  it('When hero has 1500 bullets and the castle has 750 dragon, should return true', () => {
    expect(hero(1500, 750)).toBeTrue();
  });
});
