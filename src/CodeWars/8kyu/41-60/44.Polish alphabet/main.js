export const correctPolishLetters = str => {
  const dict = new Map([
    ['ą', 'a'],
    ['ć', 'c'],
    ['ę', 'e'],
    ['ł', 'l'],
    ['ń', 'n'],
    ['ó', 'o'],
    ['ś', 's'],
    ['ź', 'z'],
    ['ż', 'z']
  ]);

  return str.split('').map(char => dict.get(char) || char).join('');
};
