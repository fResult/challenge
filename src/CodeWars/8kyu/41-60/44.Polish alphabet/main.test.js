import { correctPolishLetters } from "./main";

describe('8 Kyu: Polish alphabet',()=>{
  it('When input Jędrzej Błądziński should return Jedrzej Bladzinski',()=>{
    expect(correctPolishLetters('Jędrzej Błądziński')).toBe('Jedrzej Bladzinski')
  });
  it('When input Lech Wałęsa should return Lech Walesa',()=>{
    expect(correctPolishLetters('Lech Wałęsa')).toBe('Lech Walesa')
  });
  it('When input Maria Skłodowska-Curie should return Maria Sklodowska-Curie',()=>{
    expect(correctPolishLetters('Maria Skłodowska-Curie')).toBe('Maria Sklodowska-Curie')
  });
});
