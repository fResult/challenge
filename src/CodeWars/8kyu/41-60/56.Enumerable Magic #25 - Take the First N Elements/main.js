export const take = (arr, n) => arr.slice(0, arr.length < n ? arr.length : n);
