export const rgb = (r, g, b) => {
  function toHex(num) {
    return num <= 0 ? '00' : num < 16 ? `0${num.toString(16)}` : num > 255 ? 'FF' : num.toString(16).toUpperCase();
  }

  return [r, g, b].reduce((acc, curr) => acc + toHex(curr), '');
};

