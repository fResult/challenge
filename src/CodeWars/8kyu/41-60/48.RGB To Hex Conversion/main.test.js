import { rgb } from './main';

describe('8 Kyu: RGB To Hex Conversion', () => {
  it('When input 0, 0 and 0 should return 000000', () => {
    expect(rgb(0, 0, 0)).toBe('000000');
  });
  it('When input 0, 0 and -20 should return 000000', () => {
    expect(rgb(0, 0, -20)).toBe('000000');
  });
  it('When input 173, 255 and 47 should return ADFF2F', () => {
    expect(rgb(173, 255, 47)).toBe('ADFF2F');
  });
  it('When input 300, 255, 255 should return FFFFFF', () => {
    expect(rgb(300, 255, 255)).toBe('FFFFFF');
  });
  it('When input 148, 0 and 211 should return 9400D3', () => {
    expect(rgb(148, 0, 211)).toBe('9400D3');
  });
  it('When input 246, 3 and 207 should return F603CF', () => {
    expect(rgb(246, 3, 207)).toBe('F603CF');
  });
  it('When input 5, 247, 47 should return 05F72F', () => {
    expect(rgb(128, 2, 220)).toBe('8002DC');
  });
  it('When input 5, 247, 47 should return 8002DC', () => {
    expect(rgb(128, 2, 220)).toBe('8002DC');
  });
});
