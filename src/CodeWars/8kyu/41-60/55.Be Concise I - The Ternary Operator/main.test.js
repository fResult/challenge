import { describeAge } from './main';

describe('8 Kyu: Be Concise I - The Ternary Operator', () => {
  it('When input age is 9 should return You\'re a(n) kid', () => {
    expect(describeAge(9)).toBe('You\'re a(n) kid')
  });
  it('When input age is 10 should return You\'re a(n) kid', () => {
    expect(describeAge(10)).toBe('You\'re a(n) kid')
  });
  it('When input age is 11 should return You\'re a(n) kid', () => {
    expect(describeAge(11)).toBe('You\'re a(n) kid')
  });
  it('When input age is 12 should return You\'re a(n) kid', () => {
    expect(describeAge(12)).toBe('You\'re a(n) kid')
  });
  it('When input age is 13 should return You\'re a(n) teenager', () => {
    expect(describeAge(13)).toBe('You\'re a(n) teenager')
  });
  it('When input age is 14 should return You\'re a(n) teenager', () => {
    expect(describeAge(14)).toBe('You\'re a(n) teenager')
  });
  it('When input age is 15 should return You\'re a(n) teenager', () => {
    expect(describeAge(15)).toBe('You\'re a(n) teenager')
  });
  it('When input age is 16 should return You\'re a(n) teenager', () => {
    expect(describeAge(16)).toBe('You\'re a(n) teenager')
  });
  it('When input age is 17 should return You\'re a(n) teenager', () => {
    expect(describeAge(17)).toBe('You\'re a(n) teenager')
  });
  it('When input age is 18 should return You\'re a(n) adult', () => {
    expect(describeAge(18)).toBe('You\'re a(n) adult')
  });
  it('When input age is 19 should return You\'re a(n) adult', () => {
    expect(describeAge(19)).toBe('You\'re a(n) adult')
  });
  it('When input age is 20 should return You\'re a(n) adult', () => {
    expect(describeAge(20)).toBe('You\'re a(n) adult')
  });
  it('When input age is 63 should return You\'re a(n) adult', () => {
    expect(describeAge(63)).toBe('You\'re a(n) adult')
  });
  it('When input age is 64 should return You\'re a(n) adult', () => {
    expect(describeAge(64)).toBe('You\'re a(n) adult')
  });
  it('When input age is 65 should return You\'re a(n) elderly', () => {
    expect(describeAge(65)).toBe('You\'re a(n) elderly')
  });
  it('When input age is 66 should return You\'re a(n) elderly', () => {
    expect(describeAge(66)).toBe('You\'re a(n) elderly')
  });
  it('When input age is 100 should return You\'re a(n) elderly', () => {
    expect(describeAge(100)).toBe('You\'re a(n) elderly')
  });
});
