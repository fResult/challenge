import { noSpace } from './main';

describe('8 Kyu: Remove String Spaces', () => {
  it('When input \'8 j 8   mBliB8g  imjB8B8  jl  B\' shoudl return 8j8mBliB8gimjB8B8jlB', () => {
    expect(noSpace('8 j 8   mBliB8g  imjB8B8  jl  B')).toEqual('8j8mBliB8gimjB8B8jlB')
  });
  it('When input \'8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd\' shoudl return 88Bifk8hB8BB8BBBB888chl8BhBfd', () => {
    expect(noSpace('8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd')).toEqual('88Bifk8hB8BB8BBBB888chl8BhBfd')
  });
  it('When input \'8aaaaa dddd r     \' shoudl return 8aaaaaddddr', () => {
    expect(noSpace('8aaaaa dddd r     ')).toEqual('8aaaaaddddr')
  });
  it('When input \'\' shoudl return 8aaaaaddddr \'\'', () => {
    expect(noSpace('')).toEqual('')
  });
});
