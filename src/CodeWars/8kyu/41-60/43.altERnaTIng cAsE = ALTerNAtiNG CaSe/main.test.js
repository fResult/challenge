import { toAlternatingCase } from './main';

describe('8 Kyu: altERnaTIng cAsE <=> ALTerNAtiNG CaSe', () => {
  it('When input hello world should return HELLO WORLD', () => {
    expect(toAlternatingCase('hello world')).toBe('HELLO WORLD');
  });
  it('When input HELLO WORLD should return hello world', () => {
    expect(toAlternatingCase('HELLO WORLD')).toBe('hello world');
  });
  it('When input hello WORLD should return HELLO world', () => {
    expect(toAlternatingCase('hello WORLD')).toBe('HELLO world');
  });
  it('When input HeLLo WoRLD should return hEllO wOrld', () => {
    expect(toAlternatingCase('HeLLo WoRLD')).toBe('hEllO wOrld');
  });
  it('When input 12345 should return 12345', () => {
    expect(toAlternatingCase('12345')).toBe('12345');
  });
  it('When input 1a2b3c4d5e should return 1A2B3C4D5E', () => {
    expect(toAlternatingCase('1a2b3c4d5e')).toBe('1A2B3C4D5E');
  });
  it('When input 1A2B3C4D5E should return 1a2b3c4d5e', () => {
    expect(toAlternatingCase('1A2B3C4D5E')).toBe('1a2b3c4d5e');
  });
  it('When input String.prototype.toAlternatingCase should return sTRING.PROTOTYPE.TOaLTERNATINGcASE', () => {
    expect(toAlternatingCase('String.prototype.toAlternatingCase')).toBe('sTRING.PROTOTYPE.TOaLTERNATINGcASE');
  });
  it('When input Hello World should return Hello World', () => {
    expect(toAlternatingCase(toAlternatingCase('Hello World'))).toBe('Hello World');
  });
});
