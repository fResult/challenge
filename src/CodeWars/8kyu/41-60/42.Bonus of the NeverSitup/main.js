export const filter1stArrayFrom2ndArray = (arr1, arr2) => {
  return new Set(arr1.filter(num => arr2.includes(num)));
};
