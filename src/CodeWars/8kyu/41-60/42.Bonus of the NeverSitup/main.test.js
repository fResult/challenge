import { filter1stArrayFrom2ndArray } from './main';

describe('Bonus: NeverSitup\'s assignment', () => {
  it('When input [1, 2, 3] and [3, 1] should return set of [1, 3]', () => {
    expect(filter1stArrayFrom2ndArray([1, 2, 3], [3, 1])).toEqual(new Set([1, 3]))
  });
  it('When input [1, 2, 3, 4, 5] and [4, 5, 6] should return set of [4, 5]', () => {
    expect(filter1stArrayFrom2ndArray([1, 2, 3, 4, 5], [4, 5, 6])).toEqual(new Set([4, 5]))
  });
  it('When input [999, 101, 203, 105, 998] and [998, 101, 999, 100] should return set of [101, 998, 999]', () => {
    expect(filter1stArrayFrom2ndArray([999, 101, 203, 105, 998], [998, 101, 999, 100])).toEqual(new Set([101, 998, 999]))
  });
  it('When input [999, 101, 203, 105, 998] and [999, 998, 101, 101, 999, 100] should return set of [101, 998, 999]', () => {
    expect(filter1stArrayFrom2ndArray([999, 101, 203, 105, 998], [999, 998, 101, 101, 999, 100])).toEqual(new Set([101, 998, 999]))
  });
  it('When input [1, 1, 2, 3, 4, 5, 6, 6, 9] and [1, 3, 5, 7, 9] should return set of [1, 3, 5, 9]', () => {
    expect(filter1stArrayFrom2ndArray([1, 1, 2, 3, 4, 5, 6, 6, 9], [1, 3, 5, 7, 9])).toEqual(new Set([1, 3, 5, 9]))
  });
});
