import { _if } from './main';

describe('8 Kyu: The \'if\' function', () => {
  it('When input 1 + 1 and 2 should return 1 + 1 and 2', () => {
    expect(_if(1 + 1, 2)).toBe(1 + 1, 2)
  });
  it('When input 3 + 4 and 7 should return 3 + 4 and 7', () => {
    expect(_if(3 + 4, 7)).toBe(3 + 4, 7)
  });
  it('When input 7 + 2 and 9 should return 2 + 7 and 9', () => {
    expect(_if(7 + 2, 9)).toBe(2 + 7, 9)
  });
});
