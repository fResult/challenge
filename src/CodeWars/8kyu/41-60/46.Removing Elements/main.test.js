import { removeEveryOther } from './main';

describe('8 Kyu: Removing Elements', () => {
  it('When input [\'Hello\', \'Goodbye\', \'Hello Again\'] should return [\'Hello\', \'Hello Again\']', () => {
    expect(removeEveryOther(['Hello', 'Goodbye', 'Hello Again'])).toEqual(['Hello', 'Hello Again']);
  });
  it('When input [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] should return [1, 3, 5, 7, 9]', () => {
    expect(removeEveryOther([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])).toEqual([1, 3, 5, 7, 9]);
  });
  it('When input [[1, 2]] should return [[1, 2]]', () => {
    expect(removeEveryOther([[1, 2]])).toEqual([[1, 2]]);
  });
  it('When input [\'d\', \'s\', \'q\', \'a\', \'o\', \'e\', \'n\', \'n\', \'s\', \'n\', \'w\', \'s\', \'6\', \'l\', \'o\'] should return [\'d\', \'q\', \'o\', \'n\', \'s\', \'w\', \'6\', \'o\']', () => {
    expect(removeEveryOther(['d', 's', 'q', 'a', 'o', 'e', 'n', 'n', 's', 'n', 'w', 's', '6', 'l', 'o'])).toEqual(['d', 'q', 'o', 'n', 's', 'w', '6', 'o']);
  });
});
