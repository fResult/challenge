export const removeEveryOther = arr => arr.filter((item, idx) => !(idx % 2));
