export const monkeyCount = num => Array(num).fill().map((_, i) => i + 1);
