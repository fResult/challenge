import {differenceInAges} from './main';

describe('8 Kyu: Find the Difference in Age between Oldest and Youngest Family Members', ()=>{
  it('When input [82, 15, 6, 38, 35] should return [6, 82, 76]',()=>{
    expect(differenceInAges([82, 15, 6, 38, 35])).toEqual([6, 82, 76]);
  });
  it('When input [57, 99, 14, 32] should return [14, 99, 85]',()=>{
    expect(differenceInAges([57, 99, 14, 32])).toEqual([14, 99, 85]);
  });
});
