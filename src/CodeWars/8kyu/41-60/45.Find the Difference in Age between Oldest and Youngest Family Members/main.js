export const differenceInAges = ages => {
  ages = ages.sort((a, b) => a - b);
  const oldest = ages[ages.length - 1], youngest = ages[0];
  return [youngest, oldest, oldest - youngest]
};
