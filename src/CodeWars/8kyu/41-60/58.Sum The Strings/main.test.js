import {sumStr} from './main';

describe('8 Kyu: Sum The Strings', () => {
  it('When input \'4\' and \'5\' should return \'9\'', () => {
    expect(sumStr('4', '5')).toBe('9');
  });
  it('When input \'34\' and \'5\' should return \'39\'', () => {
    expect(sumStr('34', '5')).toBe('39');
  });
  it('When input \'538\' and \'258\' should return \'796\'', () => {
    expect(sumStr('538', '258')).toBe('796');
  });
  it('When input \'\' and \'5\' should return \'5\'', () => {
    expect(sumStr('', '5')).toBe('5');
  });
  it('When input \'517\' and \'\' should return \'517\'', () => {
    expect(sumStr('517', '')).toBe('517');
  });
  it('When input \'\' and \'\' should return \'0\'', () => {
    expect(sumStr('', '')).toBe('0');
  });
});
