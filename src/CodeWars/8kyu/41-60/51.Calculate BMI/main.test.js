import { calBmi } from './main';

describe('8 Kyu: Calculate BMI', () => {
  it('When input 80 and 1.80 should return Normal', () => {
    expect(calBmi(80, 1.80)).toBe('Normal');
  });
  it('When input 75 and 1.65 should return Overweight', () => {
    expect(calBmi(75, 1.65)).toBe('Overweight');
  });
  it('When input 112 and 1.77 should return Obese', () => {
    expect(calBmi(112, 1.77)).toBe('Obese');
  });
  it('When input 66 and 1.90 should return Underweight', () => {
    expect(calBmi(66, 1.90)).toBe('Underweight');
  });
});
