import { getDrinkByProfession } from './main';

describe('8 Kyu: L1: Bartender, drinks!', () => {
  it('When input jabrOni should return Patron Tequila', () => {
    expect(getDrinkByProfession('jabrOni')).toBe('Patron Tequila')
  });
  it('When input scHOOl counselor should return Anything with Alcohol', () => {
    expect(getDrinkByProfession('scHOOl counselor')).toBe('Anything with Alcohol')
  });
  it('When input prOgramMer should return Hipster Craft Beer', () => {
    expect(getDrinkByProfession('prOgramMer')).toBe('Hipster Craft Beer')
  });
  it('When input bike ganG member should return Moonshine', () => {
    expect(getDrinkByProfession('bike ganG member')).toBe('Moonshine')
  });
  it('When input poLiTiCian should return Your tax dollars', () => {
    expect(getDrinkByProfession('poLiTiCian')).toBe('Your tax dollars')
  });
  it('When input rapper should return Cristal', () => {
    expect(getDrinkByProfession('rapper')).toBe('Cristal')
  });
  it('When input pundit should return Beer', () => {
    expect(getDrinkByProfession('pundit')).toBe('Beer')
  });
  it('When input Pug should return Beer', () => {
    expect(getDrinkByProfession('Pug')).toBe('Beer')
  });
});
