export const getDrinkByProfession = profession => {
  const parts = profession.split(' ');
  let professionTransform = '';
  for (let i = 0; i < parts.length; i++) {
    let part = parts[i];
    part = `${part.charAt(0).toUpperCase()}${part.slice(1).toLowerCase()}`;
    professionTransform += (i === parts.length - 1) ? part :
      (part) ? `${part} ` : '';
  }

  profession = professionTransform;

  const professionWithBeer = new Map([
    ["Jabroni", "Patron Tequila"],
    ["School Counselor", "Anything with Alcohol"],
    ["Programmer", "Hipster Craft Beer"],
    ["Bike Gang Member", "Moonshine"],
    ["Politician", "Your tax dollars"],
    ["Rapper", "Cristal"],
  ]);

  for (const [key, value] of professionWithBeer) {
    if (profession === key) return value;
  }

  return 'Beer';
};
