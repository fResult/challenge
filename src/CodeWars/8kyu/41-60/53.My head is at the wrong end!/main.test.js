import { fixTheMeerkat } from './main';

describe('', () => {
  it('When input [\'tail\', \'body\', \'head\'] should return [\'head\', \'body\', \'tail\']', () => {
    expect(fixTheMeerkat(['tail', 'body', 'head'])).toEqual(['head', 'body', 'tail']);
  });
  it('When input [\'tails\', \'body\', \'heads\'] should return [\'heads\', \'body\', \'tails\']', () => {
    expect(fixTheMeerkat(['tails', 'body', 'heads'])).toEqual(['heads', 'body', 'tails']);
  });
  it('When input [\'bottom\', \'middle\', \'top\'] should return [\'top\', \'middle\', \'bottom\']', () => {
    expect(fixTheMeerkat(['bottom', 'middle', 'top'])).toEqual(['top', 'middle', 'bottom']);
  });
  it('When input [\'lower legs\', \'torso\', \'upper legs\'] should return [\'upper legs\', \'torso\', \'lower legs\']', () => {
    expect(fixTheMeerkat(['lower legs', 'torso', 'upper legs'])).toEqual(['upper legs', 'torso', 'lower legs']);
  });
  it('When input [\'ground\', \'rainbow\', \'sky\'] should return [\'sky\', \'rainbow\', \'ground\']', () => {
    expect(fixTheMeerkat(['ground', 'rainbow', 'sky'])).toEqual(['sky', 'rainbow', 'ground']);
  });
});
