import { abbrevName } from './main';

describe('8Kyu: Abbreviate a Two Word Name', () => {
  it('When input Sam Harris should return S.H', () => {
    expect(abbrevName('Sam Harris')).toBe('S.H');
  });
  it('When input Patrick Feenan should return P.F', () => {
    expect(abbrevName('Patrick Feenan')).toBe('P.F');
  });
  it('When input R Favuzzi should return R.F', () => {
    expect(abbrevName('R Favuzzi')).toBe('R.F');
  });
  it('When input korakod Boodsing return K.B', () => {
    expect(abbrevName('korakod Boodsing ')).toBe('K.B');
  });
  it('When input muhammed sihah return M.S', () => {
    expect(abbrevName('muhammed sihah')).toBe('M.S');
  });
});
