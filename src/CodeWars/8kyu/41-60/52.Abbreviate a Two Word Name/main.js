export const abbrevName = name => `${name.split(' ')[0].substring(0, 1)}.${name.split(' ')[1].substring(0, 1)}`.toUpperCase();
