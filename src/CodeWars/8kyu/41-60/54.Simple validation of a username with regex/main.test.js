import { validateUsr } from './main';

describe('8 Kyu: Simple validation of a username with regex', () => {
  it('When input asddsa should return true', () => {
    expect(validateUsr('asddsa')).toBeTrue();
  });
  it('When input a should return false', () => {
    expect(validateUsr('a')).toBeFalse();
  });
  it('When input Hass should return false', () => {
    expect(validateUsr('Hass')).toBeFalse();
  });
  it('When input Hasd_12assssssasasasasasaasasasasas should return false', () => {
    expect(validateUsr('Hasd_12assssssasasasasasaasasasasas')).toBeFalse();
  });
  it('When input \'\' should return false', () => {
    expect(validateUsr('')).toBeFalse();
  });
  it('When input ____ should return true', () => {
    expect(validateUsr('____')).toBeTrue();
  });
  it('When input asd43_34 should return true', () => {
    expect(validateUsr('asd43_34')).toBeTrue();
  });
});
