import {digit} from './main';

describe('8 Kyu: Regexp Basics - is it a digit?', () => {
  it('When input \'\' should return false', () => {
    expect(digit('')).toBeFalse();
  });
  it('When input 7 should return true', () => {
    expect(digit('7')).toBeTrue();
  });
  it('When input \' \' should return false', () => {
    expect(digit(' ')).toBeFalse();
  });
  it('When input a should return false', () => {
    expect(digit('a')).toBeFalse();
  });
  it('When input a5 should return false', () => {
    expect(digit('a5')).toBeFalse();
  });
  it('When input 14 should return true', () => {
    expect(digit('14')).toBeTrue();
  });
});
