import { strCount } from './main';

describe('8 Kyu: All Star Code Challenge #18', () => {
  it('When input Hello and o should return 1', () => {
    expect(strCount('Hello', 'o')).toBe(1);
  });
  it('When input Hello and l should return 2', () => {
    expect(strCount('Hello', 'l')).toBe(2);
  });
  it('When input \'\' and z should return 0', () => {
    expect(strCount('', 'z')).toBe(0);
  });
  it('When input dddijjzg and j should return 2', () => {
    expect(strCount('dddijjzg', 'j')).toBe(2);
  });
  it('When input izddbkabcfaeizchcjjkd and i should return 2', () => {
    expect(strCount('izddbkabcfaeizchcjjkd', 'i')).toBe(2);
  });
});
