import { gracefulTipping } from './main';

describe('7 Kyu: Graceful Tipping', () => {
  it('When input 7 should return 9', () => {
    expect(gracefulTipping(7)).toBe(9);
  });
  it('When input 1 should return 2', () => {
    expect(gracefulTipping(1)).toBe(2);
  });
  it('When input 12 should return 15', () => {
    expect(gracefulTipping(12)).toBe(15);
  });
  it('When input 86 should return 100', () => {
    expect(gracefulTipping(86)).toBe(100);
  });
  it('When input 100 should return 150', () => {
    expect(gracefulTipping(100)).toBe(150);
  });
  it('When input 10 should return 15', () => {
    expect(gracefulTipping(10)).toBe(15);
  });
  it('When input 1325 should return 2000', () => {
    expect(gracefulTipping(1325)).toBe(2000);
  });
  it('When input 99 should return 150', () => {
    expect(gracefulTipping(99)).toBe(150);
  });
  it('When input 983212 should return 1500000', () => {
    expect(gracefulTipping(983212)).toBe(1500000);
  });
  it('When input 1050000 should return 1500000', () => {
    expect(gracefulTipping(1050000)).toBe(1500000);
  });
});
