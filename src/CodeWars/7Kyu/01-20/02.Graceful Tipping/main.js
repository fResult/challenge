export const gracefulTipping = bill => {
  const billWithTip = bill * 1.15;
  const rounding = billWithTip < 10 ? 1 : 5 * 10 ** (Math.floor(Math.log10(billWithTip) - 1));
  return Math.ceil(billWithTip / rounding) * rounding;
};
