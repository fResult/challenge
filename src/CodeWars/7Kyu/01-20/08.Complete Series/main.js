export const completeSeries = arr => {
  return arr.length === new Set(arr).size ? Array(Math.max(...arr) + 1).fill().map((_, idx) => idx) : [0]
}
