import { daysRepresented } from './main'
import { describe, expect, it } from '@jest/globals'

describe('7 Kyu: How many days are we represented in a foreign country?', () => {
  it('When input [[10, 15], [25, 35]] should return 17', () => {
    expect(daysRepresented([[10, 15], [25, 35]])).toBe(17)
  })
  it('When input [[2, 8], [220, 229], [10, 16]] should return 24', () => {
    expect(daysRepresented([[2, 8], [220, 229], [10, 16]])).toBe(24)
  })
  it('When input [[44, 56], [280, 290], [185, 208], [208, 231] should return 117', () => {
    expect(daysRepresented([[44, 56], [280, 290], [185, 208], [208, 231], [251, 272], [151, 174]])).toBe(117)
  })
  it('When input [[219, 232], [247, 261], [283, 288], [198, 224], [75, 88], [226, 263]] should return 86', () => {
    expect(daysRepresented([[219, 232], [247, 261], [283, 288], [198, 224], [75, 88], [226, 263]])).toBe(86)
  })
  it('When input [[32, 69], [201, 240], [91, 125], [88, 110], [43, 81], [131, 143]] should return 141', () => {
    expect(daysRepresented([[32, 69], [201, 240], [91, 125], [88, 110], [43, 81], [131, 143]])).toBe(141)
  })
})