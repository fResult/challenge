export const daysRepresented = (trips) => {
  const set = new Set()
  trips.forEach(([start, end]) => {
    while (end >= start) {
      set.add(++start)
    }
  })
  return set.size
}