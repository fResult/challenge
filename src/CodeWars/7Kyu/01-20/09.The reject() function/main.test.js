import { reject } from './main'

describe('7 Kyu: The reject() function', () => {
  it('When input [1, 2, 3, 4, 5, 6] and (num) => num % 2 === 0 should return [1, 3, 5]', () => {
    expect(reject([1, 2, 3, 4, 5, 6], (num) => num % 2 === 0)).toEqual([1, 3, 5])
  })
  it('When input [2, 4, 6, 8, 10, 12] and (num) => num % 2 === 0 should return [2, 4, 8, 10]', () => {
    expect(reject([2, 4, 6, 8, 10, 12], (num) => num % 3 === 0)).toEqual([2, 4, 8, 10])
  })
  it('When input [\'1\', 2, \'3\', 4] and (item) => typeof item === \'number\' should return [\'1\', \'3\']', () => {
    expect(reject(['1', 2, '3', 4], (item) => typeof item === 'number')).toEqual(['1', '3'])
  })
  it('When input [\'1\', 2, \'3\', 4] and (item) => typeof item === \'string\' should return [2, 4]', () => {
    expect(reject(['1', 2, '3', 4], (item) => typeof item === 'string')).toEqual([2, 4])
  })
  it('When input [1, 2, 3, 4] and (num) => num > 0 should return []', () => {
    expect(reject([1, 2, 3, 4], (num) => num > 0)).toEqual([])
  })
})