export const solve = (str) => {
  const arr = str.split('').sort()
  return arr.every((char, idx) => {
    return idx !== 0
      ? char.charCodeAt() === arr[idx - 1].charCodeAt() + 1
      : true
  })
}
