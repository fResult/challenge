import { solve } from './main'
import { describe, expect, it } from '@jest/globals'

describe('7 Kyu: Consecutive letters', () => {
  it("When input 'abc' should return true", () => {
    expect(solve('abc')).toBeTruthy()
  })
  it("When input 'abd' should return false", () => {
    expect(solve('abd')).toBeFalsy()
  })
  it("When input 'dabc' should return true", () => {
    expect(solve('dabc')).toBeTruthy()
  })
  it("When input 'abbc' should return false", () => {
    expect(solve('abbc')).toBeFalsy()
  })
  it("When input 'zyxw' should return true", () => {
    expect(solve('zyxw')).toBeTruthy()
  })
  it("When input 'hello' should return false", () => {
    expect(solve('hello')).toBeFalsy()
  })
})
