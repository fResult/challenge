const orderedCount = require('./main')

describe('7 Kyu: Ordered Count of Characters', () => {
  it(`When input abracadabra should return [['a', 5], ['b', 2], ['r', 2], ['c', 1], ['d', 1]]`, () => {
    expect(orderedCount('abracadabra')).toEqual([['a', 5], ['b', 2], ['r', 2], ['c', 1], ['d', 1]]);
  });
  it(`When input "Code Wars" should return [['C', 1], ['o', 1], ['d', 1], ['e', 1], [' ', 1], ['W', 1], ['a', 1], ['r', 1], ['s', 1]]`, () => {
    expect(orderedCount('Code Wars')).toEqual([['C', 1], ['o', 1], ['d', 1], ['e', 1], [' ', 1], ['W', 1], ['a', 1], ['r', 1], ['s', 1]]);
  });
  it(`When input 212 should return [['2', 2], ['1', 1 ]]`, () => {
    expect(orderedCount('212')).toEqual([['2', 2], ['1', 1]]);
  });
  it(`When input 332238488 should return [['3', 3], ['2', 2], ['8', 3], ['4', 1]]`, () => {
    expect(orderedCount('332238488')).toEqual([['3', 3], ['2', 2], ['8', 3], ['4', 1]]);
  });
  it(`When input 730394711112057044008171452616301575132182868162086632901829387006750542485911686531 should return [['7', 7], ['3', 7], ['0', 11], ['9', 4], ['4', 6], ['1', 15], ['2', 8], ['5', 8], ['8', 9], ['6', 9]]`, () => {
    expect(orderedCount('730394711112057044008171452616301575132182868162086632901829387006750542485911686531')).toEqual([['7', 7], ['3', 7], ['0', 11], ['9', 4], ['4', 6], ['1', 15], ['2', 8], ['5', 8], ['8', 9], ['6', 9]]);
  });
  it(`When input '' should return []`, () => {
    expect(orderedCount('')).toEqual([]);
  });
  it(`When input kk should return [['k', 2]]`, () => {
    expect(orderedCount('kk')).toEqual([['k', 2]]);
  });
  it(`When input k should return [['k', 1]]`, () => {
    expect(orderedCount('k')).toEqual([['k', 1]]);
  });
  it(`When input kK should return [['k', 1], ['K', 1]`, () => {
    expect(orderedCount('kK')).toEqual([['k', 1], ['K', 1]]);
  });
});

