export const rowSumOddNumbers = row => {
  const nth = row / 2 * (1 + row);
  return Array(nth).fill(0)
    .map((_, idx) => (idx + 1) * 2 - 1)
    .filter((_, idx) => idx > (nth - row - 1))
    .reduce((acc, num) => acc + num, 0);
};
