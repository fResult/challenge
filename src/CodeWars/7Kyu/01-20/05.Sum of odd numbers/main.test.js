import { rowSumOddNumbers } from './main';

describe('7 Kyu: Sum of odd numbers', () => {
  it('When input 1 should return 1', () => {
    expect(rowSumOddNumbers(1)).toBe(1);
  });
  it('When input 2 should return 8', () => {
    expect(rowSumOddNumbers(2)).toBe(8);
  });
  it('When input 3 should return 27', () => {
    expect(rowSumOddNumbers(3)).toBe(27);
  });
  it('When input 4 should return 64', () => {
    expect(rowSumOddNumbers(4)).toBe(64);
  });
  it('When input 13 should return 2197', () => {
    expect(rowSumOddNumbers(13)).toBe(2197);
  });
  it('When input 19 should return 6859', () => {
    expect(rowSumOddNumbers(19)).toBe(6859);
  });
  it('When input 41 should return 68921', () => {
    expect(rowSumOddNumbers(41)).toBe(68921);
  });
  it('When input 42 should return 74088', () => {
    expect(rowSumOddNumbers(42)).toBe(74088);
  });
  it('When input 74 should return 405224', () => {
    expect(rowSumOddNumbers(74)).toBe(405224);
  });
  it('When input 86 should return 636056', () => {
    expect(rowSumOddNumbers(86)).toBe(636056);
  });
  it('When input 93 should return 804357', () => {
    expect(rowSumOddNumbers(93)).toBe(804357);
  });
  it('When input 101 should return 1030301', () => {
    expect(rowSumOddNumbers(101)).toBe(1030301);
  });
  it('When input 191 should return 6967871', () => {
    expect(rowSumOddNumbers(191)).toBe(6967871);
  });
  it('When input 222 should return 10941048', () => {
    expect(rowSumOddNumbers(222)).toBe(10941048);
  });
  it('When input 291 should return 24642171', () => {
    expect(rowSumOddNumbers(291)).toBe(24642171);
  });
});
