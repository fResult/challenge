import { isOrthogonal } from './main'

describe('7 Kyu: Orthogonal Vectors', () => {
  it('When input [1, 2] and [2, 1] should return false', () => {
    expect(isOrthogonal([1, 2], [2, 1])).toBeFalse()
  })
})
