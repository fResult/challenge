export const getSum = (a, b) => {
  let min, max, sum = 0;
  if (a >= b) {
    min = b;
    max = a;
  } else {
    min = a;
    max = b
  }

  for (let i = min; i <= max; i++) {
    sum += i;
  }
  return sum;
};
