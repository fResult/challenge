import { getSum } from './main';

describe('7 Kyu: Beginner Series #3 Sum of Numbers', () => {
  it('When input 1 and 0 should return 1', () => {
    expect(getSum(1, 0)).toEqual(1);
  });
  it('When input 10 and 0 should return 55', () => {
    expect(getSum(10, 0)).toEqual(55);
  });
  it('When input 100 and 0 should return 5050', () => {
    expect(getSum(100, 0)).toEqual(5050);
  });
  it('When input 0 and 100 should return 5050', () => {
    expect(getSum(0, 100)).toEqual(5050);
  });
  it('When input 0 and 10 should return 55', () => {
    expect(getSum(0, 10)).toEqual(55);
  });
  it('When input 3 and 50 should return 1272', () => {
    expect(getSum(3, 50)).toEqual(1272)
  });
  it('When input -5 and 5 should return 0', () => {
    expect(getSum(-5, 5)).toEqual(0)
  });
});
