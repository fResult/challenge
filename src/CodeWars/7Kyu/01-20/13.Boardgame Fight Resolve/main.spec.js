import { fightResolve } from './main'

describe('7 Kyu: Boardgame Fight Resolve', () => {
  it('When defender is \'K\' and attacker is  \'a\' then a defender should be win', () => {
    expect(fightResolve('K', 'a')).toBe('K')
  })
  it('When defender is \'A\' and attacker is  \'a\' then an attacker should be win', () => {
    expect(fightResolve('A', 'a')).toBe('a')
  })
  it('When defender is \'A\' and attacker is  \'p\' then an attacker should be win', () => {
    expect(fightResolve('A', 'p')).toBe('p')
  })
  it('When defender is \'k\' and attacker is  \'P\' then an attacker should be win', () => {
    expect(fightResolve('k', 'P')).toBe('P')
  })
  it('When defender is \'S\' and attacker is  \'P\' should return -1', () => {
    expect(fightResolve('S', 'P')).toBe(-1)
  })
  it('When defender is \'A\' and attacker is  \'K\' should return -1', () => {
    expect(fightResolve('A', 'K')).toBe(-1)
  })
  it('When defender is \'k\' and attacker is  \'s\' should return -1', () => {
    expect(fightResolve('k', 's')).toBe(-1)
  })
  it('When defender is \'p\' and attacker is  \'a\' should return -1', () => {
    expect(fightResolve('p', 'a')).toBe(-1)
  })
})
