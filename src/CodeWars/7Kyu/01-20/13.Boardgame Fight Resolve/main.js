export const fightResolve = (defender, attacker) => /[akps]{2}|[AKPS]{2}/.test(defender+attacker) ? -1 : /ka|sp|as|pk/i.test(defender+attacker) ? defender : attacker
