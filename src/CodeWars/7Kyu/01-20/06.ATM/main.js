export const solve = money => {
  let remained = money, count = 0;

  while (remained > 0) {
    remained = (remained >= 500) ? remained - 500:
      (remained >= 200) ? remained - 200 :
        (remained >= 100) ? remained - 100 :
          (remained >= 50) ? remained - 50 :
            (remained >= 20) ? remained - 20 :
              remained - 10;
    if (remained < 0) return -1;
    count++;
  }
  return count
};
