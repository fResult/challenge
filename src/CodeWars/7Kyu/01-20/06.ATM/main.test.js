import {solve} from './main';

describe('7 Kyu: ATM' ,() => {
  it('When input 770 should return 4', () => {
    expect(solve(770)).toBe(4);
  });
  it('When input 550 should return 2', () => {
    expect(solve(550)).toBe(2);
  });
  it('When input 10 should return 1', () => {
    expect(solve(10)).toBe(1);
  });
  it('When input 1250 should return 4', () => {
    expect(solve(1250)).toBe(4);
  });
  it('When input 300 should return 2', () => {
    expect(solve(300)).toBe(2);
  });
  it('When input 125 should return -1', () => {
    expect(solve(125)).toBe(-1);
  });
  it('When input 980 should return 6', () => {
    expect(solve(980)).toBe(6);
  });
  it('When input 3 should return -1', () => {
    expect(solve(3)).toBe(-1);
  });
  it('When input 1480 should return 7', () => {
    expect(solve(1480)).toBe(7);
  });
  it('When input 5000 should return 10', () => {
    expect(solve(5000)).toBe(10);
  });
  it('When input 666 should return -1', () => {
    expect(solve(666)).toBe(-1);
  });
  it('When input 42 should return -1', () => {
    expect(solve(42)).toBe(-1);
  });
  it('When input 830 should return 5', () => {
    expect(solve(830)).toBe(5);
  });
  it('When input 64 should return -1', () => {
    expect(solve(64)).toBe(-1);
  });
  it('When input 112 should return -1', () => {
    expect(solve(112)).toBe(-1);
  });
  it('When input 70 should return 2', () => {
    expect(solve(70)).toBe(2);
  });
});
