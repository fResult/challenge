export const partList = (words) =>
  words
    .map((_, idx) =>
      [words.slice(0, idx).join(' '), words.slice(idx).join(' ')]
    )
    .slice(1)
