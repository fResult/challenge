import { partList } from './main'
import { describe, expect, test } from '@jest/globals'

describe('7 Kyu: Parts of a list', () => {
  test('When input [\'I\', \'wish\', \'I\', "hadn\'t", \'come\'] should return  [[\'I\', \'wish I hadn\'t come\'], [\I wish\', \'I hadn\'t come\'], [\'I wish I\', \'hadn\'t come\'], [\'I wish I hadn\'t\', \'come\]]', () => {
    expect(partList(['I', 'wish', 'I', 'hadn\'t', 'come'])).toEqual([['I', 'wish I hadn\'t come'], ['I wish', 'I hadn\'t come'], ['I wish I', 'hadn\'t come'], ['I wish I hadn\'t', 'come']])
  })
  test('When input ["cdIw", "tzIy", "xDu", "rThG"] should return [["cdIw", "tzIy xDu rThG"], ["cdIw tzIy", "xDu rThG"], ["cdIw tzIy xDu", "rThG"]]', () => {
    expect(partList(['cdIw', 'tzIy', 'xDu', 'rThG'])).toEqual([['cdIw', 'tzIy xDu rThG'], ['cdIw tzIy', 'xDu rThG'], ['cdIw tzIy xDu', 'rThG']])
  })
  test('When input ["vJQ", "anj", "mQDq", "sOZ"] should return [["vJQ", "anj mQDq sOZ"], ["vJQ anj", "mQDq sOZ"], ["vJQ anj mQDq", "sOZ"]]', () => {
    expect(partList(["vJQ", "anj", "mQDq", "sOZ"])).toEqual([["vJQ", "anj mQDq sOZ"], ["vJQ anj", "mQDq sOZ"], ["vJQ anj mQDq", "sOZ"]])
  })
})
