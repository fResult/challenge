import { reverseBits } from './main'
import { describe, expect, it } from '@jest/globals'

describe('7 Kyu: Reverse the bits in an integer', () => {
  it('When input 417 should return 267', () => {
    expect(reverseBits(417)).toBe(267)
  })
  it('When input 267 should return 417', () => {
    expect(reverseBits(267)).toBe(417)
  })
  it('When input 0 should return 0', () => {
    expect(reverseBits(0)).toBe(0)
  })
  it('When input 2017 should return 1087', () => {
    expect(reverseBits(2017)).toBe(1087)
  })
  it('When input 1023 should return 1023', () => {
    expect(reverseBits(1023)).toBe(1023)
  })
  it('When input 1024 should return 1', () => {
    expect(reverseBits(1024)).toBe(1)
  })
  it('When input 8169591806092170 should return 2877414167907351', () => {
    expect(reverseBits(8169591806092170)).toBe(2877414167907351)
  })
})
