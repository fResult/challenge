export const reverseBits = (num) => {
  const bits = Array.from(num.toString(2))
  return parseInt(bits.reduceRight((acc, bit) => acc + bit, ''), 2)
}
