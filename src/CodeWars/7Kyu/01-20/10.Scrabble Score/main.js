export const scrabbleScore = str => {
  const criteria = [
    [['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'], 1],
    [['D', 'G'], 2],
    [['B', 'C', 'M', 'P'], 3],
    [['F', 'H', 'V', 'W', 'Y'], 4],
    [['K'], 5],
    [['J', 'X'], 8],
    [['Q', 'Z'], 10]
  ]

  const findScore = (char) => criteria.reduce((arr, items) => {
    return items[0].includes(char.toUpperCase()) ? [...arr, items[1]] : arr
  }, [])
  return Array.from(str).reduce((acc, char, idx) => {
    return acc + ((idx === str.length - 1) ? findScore(char) : `${findScore(char)},`)
  }, '')
    .split(',')
    .map(str => Number(str))
    .reduce((acc, num) => acc + num, 0)
}