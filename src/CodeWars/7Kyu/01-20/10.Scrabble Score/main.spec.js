import { scrabbleScore } from './main'

describe('7 Kyu: Scrabble Score', () => {
  it('When input \'street\' should return 6', () => {
    expect(scrabbleScore('street')).toBe(6)
  })
  it('When input \'\' should return 0', () => {
    expect(scrabbleScore('')).toBe(0)
  })
  it('When input \'cabbage\' should return 14', () => {
    expect(scrabbleScore('cabbage')).toBe(14)
  })
  it('When input \'ca bba g  e\' should return 14', () => {
    expect(scrabbleScore('ca bba g e')).toBe(14)
  })
  it('When input \'st re et\' should return 6', () => {
    expect(scrabbleScore('st re et')).toBe(6)
  })
  it('When input \'STREET\' should return 6', () => {
    expect(scrabbleScore('STREET')).toBe(6)
  })
  it('When input \'quirky\' should return 22', () => {
    expect(scrabbleScore('quirky')).toBe(22)
  })
  it('When input \'MULTIBILLIONAIRE\' should return 20', () => {
    expect(scrabbleScore('MULTIBILLIONAIRE')).toBe(20)
  })
  it('When input \'J R ATPWAZUVWIQHCINOGL\' should return 62', () => {
    expect(scrabbleScore('J R ATPWAZUVWIQHCINOGL')).toBe(62)
  })
  it('When input \'FXJQXUQWITZ\' should return 65', () => {
    expect(scrabbleScore('FXJQXUQWITZ')).toBe(65)
  })
})