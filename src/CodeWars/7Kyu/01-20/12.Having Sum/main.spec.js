import { havingSum } from './main'

describe('7 Kyu: Having Sum', () => {
  it('When input 25 should return 47', () => {
    expect(havingSum(25)).toBe(47)
  })
  it('When input 127 should return 247', () => {
    expect(havingSum(127)).toBe(247)
  })
  it('When input 38 should return 73', () => {
    expect(havingSum(38)).toBe(73)
  })
  it('When input 1 should return 1', () => {
    expect(havingSum(1)).toBe(1)
  })
  it('When input 0 should return 0', () => {
    expect(havingSum(0)).toBe(0)
  })
  it('When input 2009 should return 4010', () => {
    expect(havingSum(2009)).toBe(4010)
  })
  it('When input 9798 should return 19590', () => {
    expect(havingSum(9798)).toBe(19590)
  })
  it('When input 7021 should return 14033', () => {
    expect(havingSum(7021)).toBe(14033)
  })
  it('When input 9999 should return 19990', () => {
    expect(havingSum(9999)).toBe(19990)
  })
})
