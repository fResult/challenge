export const havingSum = (n) => n + (n > 1 ? havingSum(n >> 1) : 0)
