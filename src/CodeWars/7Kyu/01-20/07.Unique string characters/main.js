export const solve = (a, b) => {
  return [...(a + b)].reduce((acc, char) => (
    !(a.includes(char) && b.includes(char)) ? acc + char : acc
  ), '');
};
