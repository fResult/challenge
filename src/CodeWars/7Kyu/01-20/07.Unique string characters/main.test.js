import { solve } from './main';

describe('7 Kyu: Unique string characters', () => {
  it('When input xyab and xzca should return ybzc', () => {
    expect(solve('xyab', 'xzca')).toBe('ybzc');
  });
  it('When input xyabb and xzca should return ybbzc', () => {
    expect(solve('xyabb', 'xzca')).toBe('ybbzc');
  });
  it('When input xxa and axx should return \'\'', () => {
    expect(solve('xxa', 'axx')).toBe('');
  });
  it('When input abcd and xyz should return abcdxyz', () => {
    expect(solve('abcd', 'xyz')).toBe('abcdxyz');
  });
  it('When input xxx and xzca should return zca', () => {
    expect(solve('xxx', 'xzca')).toBe('zca');
  });
  it('When input abcd and wxyz should return abcdwxyz', () => {
    expect(solve('abcd', 'wxyz')).toBe('abcdwxyz');
  });
});
