export const multiples = (m, n) =>
  Array(m)
    .fill()
    .reduce((acc, _, idx) => [...acc, (idx + 1) * n], [])
