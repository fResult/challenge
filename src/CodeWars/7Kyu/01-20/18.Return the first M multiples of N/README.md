7 Kyu: Return the first M multiples of N
==
Description:
-Implement a function, multiples(m, n), which returns an array of the first m multiples of the real number n. Assume that m is a positive integer.

### Ex.
```
multiples(3, 5)
```

#### should return
```
[5, 10, 15]
```

Source:
--
https://www.codewars.com/kata/593c9175933500f33400003e/train/javascript
