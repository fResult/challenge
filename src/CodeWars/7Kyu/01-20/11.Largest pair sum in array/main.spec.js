import { largestPairSum } from './main'

describe('7 Kyu: Largest pair sum in array', () => {
  it('When input [-10, -8, -16, -18, -19] should return -18', () => {
    expect(largestPairSum([-10, -8, -16, -18, -19])).toBe(-18)
  })
  it('When input [10, 14, 2, 23, 19] should return 42', () => {
    expect(largestPairSum([10, 14, 2, 23, 19])).toBe(42)
  })
  it('When input [99, 2, 2, 23, 19] should return 122', () => {
    expect(largestPairSum([99, 2, 2, 23, 19])).toBe(122)
  })
})