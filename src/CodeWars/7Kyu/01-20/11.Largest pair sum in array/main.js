export const largestPairSum = (numbers) => {
  const [mostNumber, almostNumber] = numbers.sort((a, b) => b - a)
  return mostNumber + almostNumber
}
