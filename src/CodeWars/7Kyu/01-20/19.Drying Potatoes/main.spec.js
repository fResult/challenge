import { potatoes } from './main'
import { describe, expect, it } from '@jest/globals'

describe('7 Kyu: Drying Potatoes', () => {
  it('When input 99, 100 and 98 should return 50', () => {
    expect(potatoes(99, 100, 98)).toBe(50)
  })
  it('When input 82, 127 and 80 should return 114', () => {
    expect(potatoes(82, 127, 80)).toBe(114)
  })
  it('When input 93, 129, 91 should return 100', () => {
    expect(potatoes(93, 129, 91)).toBe(100)
  })
})
