import solve from './main'
import { describe, expect, it } from '@jest/globals'

describe('7 Kyu: Fix string case', () => {
  it('should return "code" when input "coDe"', () => {
    expect(solve('coDe')).toBe('code')
  })
  it('should return "CODE" when input "CODe"', () => {
    expect(solve('CODe')).toBe('CODE')
  })
  it('should return "code" when input "coDe"', () => {
    expect(solve('coDe')).toBe('code')
  })
  it('should return "qzoivgcroqktmuenginovjfgwdnxbpjddjlyuuobyzzbkrjyigrdqnyssldwovdlrsagquumbomvhlqznvymwzgyjzxuhklfrfqw" when input "qzOIvgCrOQKTmUeNginovjfgwdnxBPJdDJlYuuOByZzBKrjYIgRdqnySSlDWoVDlRsAGQUumbOmVhlqzNVymWZgYJZxUhKlFrfQw"', () => {
    expect(solve('qzOIvgCrOQKTmUeNginovjfgwdnxBPJdDJlYuuOByZzBKrjYIgRdqnySSlDWoVDlRsAGQUumbOmVhlqzNVymWZgYJZxUhKlFrfQw')).toBe('qzoivgcroqktmuenginovjfgwdnxbpjddjlyuuobyzzbkrjyigrdqnyssldwovdlrsagquumbomvhlqznvymwzgyjzxuhklfrfqw')
  })
  it('should return "tdqkjyungmebybxdqngdsfqezmkvcxlxmkdxmnnhpdgocgupjyohfmsrtqbkufmxwagwxhenqbuefbwmbvuvnbvtdsehdoqrnhlr" when input "tdqkjyungmebybxdqngdsfqezmkvcxlxmkdxmnnhpdgocgupjyohfmsrtqbkufmxwagwxhenqbuefbwmbvuvnbvtdsehdoqrnhlr"', () => {
    expect(solve('tdQKjYUnGmEBYbXdqNgdsFqEZmkVcxlXMKDxmnNHpdgocGupjyohFMSRTqbkUfMxWagwxhENQBuEfbwMBvuVnbVTDSEhDoQRNhLr')).toBe('tdqkjyungmebybxdqngdsfqezmkvcxlxmkdxmnnhpdgocgupjyohfmsrtqbkufmxwagwxhenqbuefbwmbvuvnbvtdsehdoqrnhlr')
  })
})
