export default (str) => {
  const [upper, lower] = str.split('').reduce(

    ([upper, lower], char) => {
      return char === char.toUpperCase()
        ? [upper + 1, lower]
        : [upper, lower + 1]
    },
    [0, 0]
  )
  return upper > lower ? str.toUpperCase() : str.toLowerCase()
}
