import { overTheRoad } from './main'
import { describe, it, expect } from '@jest/globals'

describe('7 Kyu: Over The Road', () => {
  it('should return opposite address is 6 when input your address is 1 and length of street is 3', () => {
    expect(overTheRoad(1, 3)).toBe(6)
  })

  it('should return opposite address is 4 when input your address is 3 and length of street is 3', () => {
    expect(overTheRoad(3, 3)).toBe(4)
  })

  it('should return opposite address is 5 when input your address is 2 and length of street is 3', () => {
    expect(overTheRoad(2, 3)).toBe(5)
  })

  it('should return opposite address is 8 when input your address is 3 and length of street is 5', () => {
    expect(overTheRoad(3, 5)).toBe(8)
  })

  it('should return opposite address is 16 when input your address is 7 and length of street is 11', () => {
    expect(overTheRoad(7, 11)).toBe(16)
  })

  it('should return opposite address is 1999981 when input your address is 20 and length of street is 1000000', () => {
    expect(overTheRoad(20, 1000000)).toBe(1999981)
  })

  it('should return opposite address is 144944000822 when input your address is 78759261165 and length of street is 111851630993', () => {
    expect(overTheRoad(78759261165, 111851630993)).toBe(144944000822)
  })
})
