const getTurkishNumber = require('./main')
const { describe, it } = require('@jest/globals')

describe('7 Kyu: Turkish Numbers, 0-99', () => {
  it('should return sıfır when input is 0', () => {
    expect(getTurkishNumber(0)).toBe('sıfır')
  })
  it('should return on altı when input is 16', () => {
    expect(getTurkishNumber(16)).toBe('on altı')
  })
  it('should return yetmiş when input is 70', () => {
    expect(getTurkishNumber(70)).toBe('yetmiş')
  })
  it('should return yirmi altı when input is 26', () => {
    expect(getTurkishNumber(26)).toBe('yirmi altı')
  })
  it('should return on when input is 10', () => {
    expect(getTurkishNumber(10)).toBe('on')
  })
})
