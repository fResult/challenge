const getTurkishNumber = (num) => {
  const digits = new Map([
    ["0", "sıfır"],
    ["1", "bir"],
    ["2", "iki"],
    ["3", "üç"],
    ["4", "dört"],
    ["5", "beş"],
    ["6", "altı"],
    ["7", "yedi"],
    ["8", "sekiz"],
    ["9", "dokuz"],
  ]);

  const tens = new Map([
    ["10", "on"],
    ["20", "yirmi"],
    ["30", "otuz"],
    ["40", "kırk"],
    ["50", "elli"],
    ["60", "altmış"],
    ["70", "yetmiş"],
    ["80", "seksen"],
    ["90", "doksan"],
  ]);

  return [...String(num)].reduce((acc, digit, idx) => {
    if (num >= 10) {
      if (idx === 0) {
        return tens.get(digit + 0);
      } else {
        if (idx === 1 && !parseInt(digit))
          return acc
        else
          return `${acc} ${digits.get(digit)}`;
      }
    } else {
      return digits.get(digit);
    }
  }, "");
};


module.exports = getTurkishNumber
