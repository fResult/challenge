export const spacey = (array) => {
  return array.reduce((acc, word, idx) => {
    const newWord =
      `${idx === 0 ? word : ''}${(acc + word).split(',')[idx - 1] || ''}`
    return [...acc, newWord]
  }, [])
}
