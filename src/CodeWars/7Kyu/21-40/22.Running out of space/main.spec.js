import { spacey } from './main'
import { describe, expect, it } from '@jest/globals'

describe('7 Kyu: Running out of space', () => {
  it(`should return ['kevin', '[kevinhas', 'kevinhasno', 'kevinhasnospace'] when input ['kevin', 'has','no','space']`, ()=>{
    expect(spacey(['kevin', 'has','no','space'])).toEqual(['kevin', 'kevinhas', 'kevinhasno', 'kevinhasnospace'])
  })
  it(`should return ['this','thischeese','thischeesehas','thischeesehasno','thischeesehasnoholes'] when input ['this','cheese','has','no','holes']`, ()=>{
    expect(spacey(['this','cheese','has','no','holes'])).toEqual(['this','thischeese','thischeesehas','thischeesehasno','thischeesehasnoholes'])
  })
})
