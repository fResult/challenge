export function main(str) {
	// Your code begins here;
  const pattern = /(\(\[?\]?\))|(\(\{?\}?\))|(\{[\(\)\[\]\{\}]*\})|(\[[\(\)\[\]\{\}]*\])/g
  return str.match(pattern).join('').includes(str)
}
