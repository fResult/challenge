function main(str) {
  // Your code begins here;
  let count = 0;
  let result = '';
  for (let i = 0; i < str.length; i++) {
    if (i > 0) {
      if (str[i] === str[i - 1] && i === str.length - 1) {
        result += `${count + 1}${str[i]}`
      } else if (str[i] !== str[i - 1] && i === str.length - 1) {
        result += `${count}${str[i - 1]}1${str[i]}`
      } else if (str[i] === str[i - 1]) {
        ++count;
      } else {
        result += `${count}${str[i - 1]}`;
        count = 1;
      }
    } else if (i === 0) {
      if (str.length === 1) {
        result += 1 + str[i]
      }
      count = 1
    }
  }
  return result;
}

module.exports = { main };

