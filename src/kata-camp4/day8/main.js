function main(arr, w, x, y, z) {
  const idxs = [];
  for (let i = 0; i < arr.length; i++) {
    arr[i] === w ? idxs.push(i) : undefined;
    arr[i] === x ? idxs.push(i) : undefined;
    arr[i] === y ? idxs.push(i) : undefined;
    arr[i] === z ? idxs.push(i) : undefined;
  }
  for (let i = idxs.length - 1; i >= 0; i--) {
    arr.splice(idxs[i], 1);
  }
  return arr;
}

module.exports = { main };
