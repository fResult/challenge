import {say} from "./fizzbuzz.js";

describe('ลองทำ fizzbuzz test ด้วย dictionary กันหน่อย', () => {
	test('ให้ป้อนหมายเลข 3 เข้าไปใน function say() แล้วต้องคืนค่า fizz', () => {
		expect(say(3)).toBe('fizz')
	});
	test('ให้ป้อนหมายเลข 5 เข้าไปใน function say() แล้วต้องคืนค่า buzz', () => {
		expect(say(5)).toBe('buzz')
	});
	test('ให้ป้อนหมายเลข 3 เข้าไปใน function say() แล้วต้องคืนค่า fizz', () => {
		expect(say(6)).toBe('fizz')
	});
	test('ให้ป้อนหมายเลข 10 เข้าไปใน function say() แล้วต้องคืนค่า buzz', () => {
		expect(say(10)).toBe('buzz')
	});
	test('ให้ป้อนหมายเลข 15 เข้าไปใน function say() แล้วต้องคืนค่า fizzbuzz', () => {
		expect(say(15)).toBe('fizzbuzz')
	});
	test('ให้ป้อนหมายเลข 11 เข้าไปใน function say() แล้วต้องคืนค่า 11', () => {
		expect(say(11)).toBe(11)
	});
	test('ให้ป้อนหมายเลข 45 เข้าไปใน function say() แล้วต้องคืนค่า fizzbuzz', () => {
		expect(say(45)).toBe('fizzbuzz')
	});
	test('ให้ป้อนหมายเลข 23 เข้าไปใน function say() แล้วต้องคืนค่า 23', () => {
		expect(say(23)).toBe(23)
	});
});
