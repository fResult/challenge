export function say(number) {
  const fizzBuzz = new Map([
    [true, number],
    [number % 3 === 0, 'fizz'],
    [number % 5 === 0, 'buzz'],
    [number % 3 === 0 && number % 5 === 0, 'fizzbuzz']
  ]);
  return fizzBuzz.get(true);
}
