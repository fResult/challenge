// Init+each+hash -> mash
/* ======================================= */
const items = ['functional', 'programming', 'rules'];

let hash = {};
items.forEach(item => {
  hash[item] = item.length;
});

console.log(hash);
/* ========================================================================= */
Array.prototype.mash = function (callback) {
  const addKeyValuePair = (acc, item) => {
    const [key, value] = callback ? callback(item) : item;
    return { ...acc, [key]: value };
  };

  return this.reduce(addKeyValuePair, {});
};

console.log(items.mash(item => [item, item.length]));
console.log(items.map(item => [item.length, item]).mash());
