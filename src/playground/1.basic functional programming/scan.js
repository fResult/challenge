// Init+each+accumulate+push -> scan
/* ======================================= */
const items = ['functional', 'programming', 'rules'];

let totalLength = 0;
let accumulateLengths1 = [0];

for (const item of items) {
  totalLength += item.length;
  accumulateLengths1 = [...accumulateLengths1, totalLength];
}
console.log(accumulateLengths1);

/* ========================================================================= */

Array.prototype.scan = function(callback, initialValue) {
  const appendAggregate = (acc, item) => {
    const aggregate = acc[acc.length-1];
    const newAggregate = callback(aggregate, item);
    return [...acc, newAggregate]
  };

  const accumulator = [initialValue];
  return this.reduce(appendAggregate, accumulator)
};
const accumulateLengths2 = items.scan((acc, item) => acc + item.length, 0);
console.log(accumulateLengths2);
