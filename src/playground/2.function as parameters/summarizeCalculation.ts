type summary = {
  summation: number
  factorial: number
  power: number
}

export default function summarizeCalculation(
  summation: number,
  factorial: number,
  power: number
): summary {
  return {
    summation,
    factorial,
    power
  }
}
