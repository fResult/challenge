export default function aggregate(
  initialValue: number,
  action: (total: number, num: number) => number,
  n: number
) {
  let total = initialValue

  for (let i = 1; i <= n; i++) {
    total = action(total, i)
  }

  return total
}