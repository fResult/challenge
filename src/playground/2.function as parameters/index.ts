import aggregate from './aggregate'
import summarizeCalculation from './summarizeCalculation'

const add = (x: number, y: number): number => x + y
const multiply = (x: number, y: number): number => (x === 0 ? 1 : x * y)
const power = (num: number) => (x: number, y: number): number => {
  return x === 0 ? num : x * 2
}

const hundredTermsOfSummationSeries = aggregate(0, add, 100)
const factorial5 = aggregate(0, multiply, 5)
const twoPowFour = aggregate(0, power(2), 4)

function main() {
  const { power, factorial, summation } = summarizeCalculation(
    hundredTermsOfSummationSeries,
    factorial5,
    twoPowFour
  )

  console.log('2⁴  =', power)
  console.log('1 + 2 + 3 + ... + 100  =', summation)
  console.log('5!  =', factorial)
}

main()
