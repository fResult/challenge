export function main(num) {
  let result = '';

  for (let i = 1; i <= num; i++) {
    for (let j = num; j >= 1; j--) {
      result += j > i ? '_' : '*';
    }
    i !== num ? result += '\n' : undefined;
  }

  return result;
}
