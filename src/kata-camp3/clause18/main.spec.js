import main from './main';

describe('ให้มีการกรอกตัวเลขใดๆ เข้าไปแล้วมีการคืนค่าที่คาดหวังออกมา', () => {
  test('ให้กรอกเลข 0 แล้วควรมีการคืนค่า \'\'', () => {
    expect(main(0)).toBe('');
  });
  test('ให้กรอกเลข 1 แล้วควรมีการคืนค่า *', () => {
    expect(main(1)).toBe('*');
  });
  test('ให้กรอกเลข 2 แล้วควรมีการคืนค่า **\n_*', () => {
    expect(main(2)).toBe('**\n_*');
  });
  test('ให้กรอกเลข 3 แล้วควรมีการคืนค่า ***\n_**\n__*', () => {
    expect(main(3)).toBe('***\n_**\n__*');
  });
  test('ให้กรอกเลข 4 แล้วควรมีการคืนค่า ****\n_***\n__**\n___*', () => {
    expect(main(4)).toBe('****\n_***\n__**\n___*');
  });
  test('ให้กรอกเลข 5 แล้วควรมีการคืนค่า *****\n_****\n__***\n___**\n____*', () => {
    expect(main(5)).toBe('*****\n_****\n__***\n___**\n____*');
  });
  test('ให้กรอกเลข 6 แล้วควรมีการคืนค่า ******\n_*****\n__****\n___***\n____**\n_____*', () => {
    expect(main(6)).toBe('******\n_*****\n__****\n___***\n____**\n_____*');
  });
});
