export function main(num) {
  const length = num * 2 - 1, middle = Math.ceil(length / 2), range = [middle, middle];
  let [lowerBound, upperBound] = range, counter = 1, result = '';

  for (let i = 1; i <= num; i++, lowerBound--, upperBound++) {
    for (let j = 1; j <= length; j++) {
      result += (j >= lowerBound && j <= upperBound) ? counter++ : '-';

    }
    i !== length ? result += '\n' : undefined;
  }
  lowerBound += 2;
  upperBound -= 2;
  for (let i = num; i > 1; i--, lowerBound++, upperBound--) {
    for (let j = 1; j <= length; j++) {
      result += (j >= lowerBound && j <= upperBound) ? counter++ : '-';
    }
    i !== 2 ? result += '\n' : undefined;
  }

  return result;
}
