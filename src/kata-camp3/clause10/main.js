export function main(num) {
  let result = '';

  for (let i = 1; i <= num; i++) {
    for (let j = 1; j <= num; j++) {
      result += j*i;
    }
    i !== num ? result += '\n' : undefined;
  }

  return result;
}
