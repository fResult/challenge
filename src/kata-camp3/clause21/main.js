export function main(num) {
  const length = num * 2 - 1, middle = Math.ceil(length / 2), range = [middle, middle];
  let result = '', [lowerBound, upperBound] = range;

  for (let i = 1; i <= num; i++, upperBound++, lowerBound--) {
    for (let j = 1; j <= length; j++) {
      result += (j <= upperBound && j >= lowerBound) ? '*' : '_';
    }
    i !== num ? result += '\n' : undefined;
  }

  return result;
}
