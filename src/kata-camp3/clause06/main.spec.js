import { main } from './main';

describe('ให้มีการกรอกตัวเลขใดๆ เข้าไปแล้วมีการคืนค่าที่คาดหวังออกมา', () => {
  test('ให้กรอกเลข 0 แล้วควรมีการคืนค่า \'\'', () => {
    expect(main(0)).toBe('');
  });
  test('ให้กรอกเลข 1 แล้วควรมีการคืนค่า 1', () => {
    expect(main(1)).toBe('1');
  });
  test('ให้กรอกเลข 2 แล้วควรมีการคืนค่า 12\n34', () => {
    expect(main(2)).toBe('12\n34');
  });
  test('ให้กรอกเลข 3 แล้วควรมีการคืนค่า 123\n456\n789', () => {
    expect(main(3)).toBe('123\n456\n789');
  });
  test('ให้กรอกเลข 4 แล้วควรมีการคืนค่า 1234\n5678\n9101112\n13141516', () => {
    expect(main(4)).toBe('1234\n5678\n9101112\n13141516');
  });
  test('ให้กรอกเลข 5 แล้วควรมีการคืนค่า 12345\n678910\n1112131415\n1617181920\n2122232425', () => {
    expect(main(5)).toBe('12345\n678910\n1112131415\n1617181920\n2122232425');
  });
});
