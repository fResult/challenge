import { main } from './main';

describe('ให้มีการกรอกตัวเลขใดๆ เข้าไปแล้วมีการคืนค่าที่คาดหวังออกมา', () => {
  it('ให้กรอกเลข 0 แล้วควรมีการคืนค่า \'\'', () => {
    expect(main(0)).toBe('')
  });
  it('ให้กรอกเลข 1 แล้วควรมีการคืนค่า *', () => {
    expect(main(1)).toBe('*')
  });
  it('ให้กรอกเลข 2 แล้วควรมีการคืนค่า _*_\n***\n_*_', () => {
    expect(main(2)).toBe('_*_\n***\n_*_')
  });
  it('ให้กรอกเลข 3 แล้วควรมีการคืนค่า __*__\n_***_\n*****\n_***_\n__*__', () => {
    expect(main(3)).toBe('__*__\n_***_\n*****\n_***_\n__*__')
  });
  it('ให้กรอกเลข 4 แล้วควรมีการคืนค่า ___*___\n__***__\n_*****_\n*******\n_*****_\n__***__\n___*___', () => {
    expect(main(4)).toBe('___*___\n__***__\n_*****_\n*******\n_*****_\n__***__\n___*___')
  });
  it('ให้กรอกเลข 5 แล้วควรมีการคืนค่า ____*____\n___***___\n__*****__\n_*******_\n*********\n_*******_\n__*****__\n___***___\n____*____', () => {
    expect(main(5)).toBe('____*____\n___***___\n__*****__\n_*******_\n*********\n_*******_\n__*****__\n___***___\n____*____')
  });
  it('ให้กรอกเลข 6 แล้วควรมีการคืนค่า _____*_____\n____***____\n___*****___\n__*******__\n_*********_\n***********\n_*********_\n__*******__\n___*****___\n____***____\n_____*_____', () => {
    expect(main(6)).toBe('_____*_____\n____***____\n___*****___\n__*******__\n_*********_\n***********\n_*********_\n__*******__\n___*****___\n____***____\n_____*_____')
  });
});
