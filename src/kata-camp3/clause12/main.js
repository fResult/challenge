export function main(num) {
  let result = '';

  for (let i = num; i >= 1; i--) {
    for (let j = 1; j <= num; j++) {
      result = (i === j ? '_' : '*') + result;
    }
    i !== 1 ? result = '\n' + result : undefined;
  }
  return result;
}
