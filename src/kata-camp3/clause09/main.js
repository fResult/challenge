export function main(num) {
  let result = '', counter = 2;

  for (let i = 1; i <= num; i++, counter = counter + 2) {
    result += i !== num ? counter + '\n' : counter;
  }

  return result;
}
