export function main(num) {
  let result = '', counter = num;

  if (num === 0) return '';
  else if (num === 1) return '*';

  for (; counter >= 1; counter--) {
    for (let j = 1; j <= num; j++) {
      result += j < counter ? '_' : '*';
    }
    result += '\n';
  }

  counter += 2;

  for (; counter <= num; counter++) {
    for (let j = 1; j <= num; j++) {
      result += j > counter - 1 ? '*' : '_';
    }
    counter < num ? result += '\n' : undefined;
  }

  return result;
}
