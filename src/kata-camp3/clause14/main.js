export function main(num) {
  let result = '';
  // return num === 2 ? '**\n*_' : num === 3 ? '***\n**_\n*__' : '****\n***_\n**__\n*___';

  for (let i = num; i >= 1; i--) {
    for (let j = 1; j <= num; j++) {
      result += j <= i ? '*' : '_';
    }
    i !== 1 ? result += '\n' : undefined;
  }

  return result;
}

console.log(main(5));
