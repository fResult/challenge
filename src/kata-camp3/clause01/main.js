export function main(num) {
  let result = '';

  for (let i = 1; i <= num; i++) {
    result += '*';
  }

  return result;
}
