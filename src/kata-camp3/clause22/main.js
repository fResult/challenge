export function main(num) {
  const length = num * 2 - 1, useRange = (upperBound, lowerBound) => {
    return [upperBound, lowerBound];
  };
  let result = '', [lowerBound, upperBound] = useRange(1, length);

  for (let i = 1; i <= num; i++, lowerBound++, upperBound--) {
    for (let j = length; j >= 1; j--) {
      result += (j >= lowerBound && j <= upperBound) ? '*' : '_';
    }
    i !== num ? result += '\n' : undefined;
  }

  return result;
}
