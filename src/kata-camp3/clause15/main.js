export function main(num) {
  let result = '';
  let counter = 1;

  for (; counter <= num; counter++) {
    for (let j = 1; j <= num; j++) {
      result += j <= counter ? '*' : '_';
    }
    num !== 1 ? result += '\n' : undefined;
  }

  counter -= 2;

  for (; counter >= 1; counter--) {
    for (let j = 1; j <= num; j++) {
      result += j <= counter ? '*' : '_';
    }
    counter !== 1 ? result+='\n' : undefined;
  }

  return result;
}
