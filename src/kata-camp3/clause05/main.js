export function main(num) {
  let result = '';

  for (let i = num; i >= 1; i--) {
    for (let j = num; j >= 1; j--) {
      result += i;
    }
    i !== 1 ? result += '\n' : undefined;
  }

  return result;
}
