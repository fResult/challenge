export function main(num) {
  let result = '', counter = 1;

  for (let i = num; i >= 1; i--) {
    for (let j = 1; j <= num; j++, j < i) {
      if (i > j) result += '-';
      else {
        result += counter;
        counter++;
      }
    }
    num !== 1 ? result += '\n' : undefined;
  }

  for (let i = 2; i <= num; i++) {
    for (let j = 1; j <= num; j++) {
      if (i > j) result += '-';
      else {
        result += counter;
        counter++;
      }
    }
    i < num ? result += '\n' : undefined;
  }

  return result;
}
