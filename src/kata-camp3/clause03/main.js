export function main(num) {
  let result = '';

  for (let i = 1; i <= num; i++) {
    for (let j = 1; j <= num; j++) {
      result += j;
    }
    if (i !== num) {
      result += '\n';
    }
  }

  return result;
}
